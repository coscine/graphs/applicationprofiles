Note: the Review can be carried out either by checking the `index.ttl` file under the ![changes](https://git.rwth-aachen.de/coscine/graphs/applicationprofiles/-/raw/master/.gitlab/data/changes.png) tab or by copy-and-pasting the content of the file into the [AIMS Frontend](https://coscine.rwth-aachen.de/coscine/apps/aimsfrontend/#/graph)

- [ ] Copy and paste code into [AIMS Frontend](https://coscine.rwth-aachen.de/coscine/apps/aimsfrontend/#/graph). Check the form using under the ![metadata-form](https://git.rwth-aachen.de/coscine/graphs/applicationprofiles/-/raw/master/.gitlab/data/metadata-form.png?inline=false){width=90px} button. Does everything render correctly and do all the fields work?
- [ ] Do the title and description match 
- [ ] Is there an English and German version of the title and description? (English is OK, but bi-lingual is preferred.)
- [ ] Are various datatypes used? (If applicable, in some cases, only using `string` is entirely appropriate.)
- [ ] Does each field have a description?
- [ ] If pre-defined terms are used, do they make sense? (broadly speaking, e.g., `dcterms:author` is not used for a field named "Device".)
- [ ] Does `sh:targetClass` URL match the base URL? (Applies in most but not all cases.)
- [ ] If the base URL (line one in `index.ttl`) contains the GUID such as `https://purl.org/coscine/ap/{GUID}/`, modify this to contain the name of the profile and, if required, the grouping: `https://purl.org/coscine/ap/{profileName}/` or `https://purl.org/coscine/ap/{group}/{profileName}/`
    - [ ] Change any other instances of the base URL to match.
- [ ] Deploy on DSP-10 by clicking the ![arrows](https://git.rwth-aachen.de/coscine/graphs/applicationprofiles/-/raw/master/.gitlab/data/arrows.png) in the pipeline box at the top of the merge request–just under the the ![reactions](https://git.rwth-aachen.de/coscine/graphs/applicationprofiles/-/raw/master/.gitlab/data/reactions.png). (add label accordingly when done)
- [ ] Test on DSP-10 by creating a resource with the application profile and adding a file/linked data to the resource on the web interface (add label accordingly when done). Does everything render correctly and do all the fields work?
- [ ] If applicable, contact submitter via service desk ticket (check box here and add "waiting" label to request).
- [ ] Once review is complete, comment and tag [Petar](https://git.rwth-aachen.de/petar.hristov) and [Benedikt](https://git.rwth-aachen.de/Heinrichs) to approve and merge

For more details, see the [documentation](https://docs.coscine.de/en/admins/ap_workflow/).
