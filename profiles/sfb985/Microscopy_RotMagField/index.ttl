@base <https://purl.org/coscine/ap/sfb985/Microscopy_RotMagField/> .

@prefix dash: <http://datashapes.org/dash#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix sfb985: <http://purl.org/coscine/terms/sfb985#> .
@prefix afr: <http://purl.allotrope.org/ontologies/result#> .
@prefix obo: <http://purl.obolibrary.org/obo/> .
@prefix repr: <https://w3id.org/reproduceme#> .
@prefix om2: <http://www.ontology-of-units-of-measure.org/resource/om-2/> .
@prefix ebucore: <http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#> .
@prefix coscineMicroscopy_RotMagField: <https://purl.org/coscine/ap/sfb985/Microscopy_RotMagField#> .


# Microscopy_RotMagField
<https://purl.org/coscine/ap/sfb985/Microscopy_RotMagField/>
  dcterms:license <http://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://itc.rwth-aachen.de/> ;
  dcterms:rights "Copyright © 2022 IT Center, RWTH Aachen University" ;
  dcterms:title "Microscopy in a Rotating Magnetic Field"@en, "Mikroskopie in einem rotierendem Magnetfeld"@de ;

  a sh:NodeShape ;
  sh:targetClass <https://purl.org/coscine/ap/sfb985/Microscopy_RotMagField/> ;
  sh:closed true ; 

  sh:property [
    sh:path rdf:type ;
  ] ;

  sh:property coscineMicroscopy_RotMagField:creator ;
  sh:property coscineMicroscopy_RotMagField:contributor ;
  sh:property coscineMicroscopy_RotMagField:measurementDateTime ;
  sh:property coscineMicroscopy_RotMagField:productionMethod ;
  sh:property coscineMicroscopy_RotMagField:sampleID ;
  sh:property coscineMicroscopy_RotMagField:samplePID;
  sh:property coscineMicroscopy_RotMagField:seriesID ;
  sh:property coscineMicroscopy_RotMagField:solvent ;
  sh:property coscineMicroscopy_RotMagField:fieldStrength ;
  sh:property coscineMicroscopy_RotMagField:rotationRate ;
  sh:property coscineMicroscopy_RotMagField:rotation ;
  sh:property coscineMicroscopy_RotMagField:frameRate ;
  sh:property coscineMicroscopy_RotMagField:microscope ;
  sh:property coscineMicroscopy_RotMagField:objectiveLens ;
  sh:property coscineMicroscopy_RotMagField:camera ;
  sh:property coscineMicroscopy_RotMagField:comment .

coscineMicroscopy_RotMagField:creator 
  sh:path dcterms:creator ;
  sh:order 0 ;
  sh:minCount 1 ;
  sh:minLength 1 ;
  sh:datatype xsd:string ;
  sh:name "Creator"@en, "Ersteller"@de .

coscineMicroscopy_RotMagField:contributor
  sh:path dcterms:contributor ;
  sh:order 1 ;
  sh:datatype xsd:string ;
  sh:name "Student"@en, "Student"@de . 

coscineMicroscopy_RotMagField:measurementDateTime 
  sh:path afr:AFR_0000952e ;
  sh:order 2 ;
  sh:minCount 1 ;
  sh:datatype xsd:dateTime ;
  sh:name "Date and time of measurement"@en, "Messdatum und -zeit"@de .

coscineMicroscopy_RotMagField:productionMethod 
  sh:path obo:CHMO_0001301 ;
  sh:order 3 ;
  sh:minCount 1 ;
  sh:name "Production method"@en, "Produktionsmethode"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/productionMethod>  .

coscineMicroscopy_RotMagField:sampleID
  sh:path afr:AFR_0001118 ;
  sh:order 4 ;
  sh:minCount 1 ;
  sh:datatype xsd:string ; 
  sh:name "Sample ID"@en, "Proben ID"@de .

coscineMicroscopy_RotMagField:samplePID
  sh:path dcterms:identifier ;
  sh:order 5 ;
  sh:datatype xsd:string ; 
  sh:name "SFB 985 Sample Management PID"@en, "SFB 985 Probenmanagement PID"@de .

coscineMicroscopy_RotMagField:seriesID
  sh:path repr:Experiment ;
  sh:order 6 ;
  sh:minCount 1 ;
  sh:datatype xsd:string ; 
  sh:name "Measurement Series"@en, "Messserie"@de .


coscineMicroscopy_RotMagField:solvent
  sh:path obo:CHEBI_46787 ;
  sh:order 7 ;
  sh:minCount 1 ;
  sh:datatype xsd:string ; 
  sh:name "Solvent"@en, "Lösungsmittel"@de .

coscineMicroscopy_RotMagField:fieldStrength
  sh:path om2:MagneticField ;
  sh:order 8 ;
  sh:minCount 1 ;
  sh:datatype xsd:decimal ; 
  sh:name "Feldstärke [mT]"@de, "Field strength [mT]"@en ;
  qudt:Unit unit:MilliT .

coscineMicroscopy_RotMagField:rotationRate
  sh:path obo:AFR_0000954 ;
  sh:order 9 ;
  sh:minCount 1 ;
  sh:datatype xsd:decimal ; 
  sh:name "Rotation rate [gps]"@en, "Rotationsrate [gps]"@de ;
  qudt:Unit unit:DEG-PER-SEC .

coscineMicroscopy_RotMagField:frameRate
  sh:path ebucore:frameRate ;
  sh:order 10 ;
  sh:minCount 1 ;
  sh:datatype xsd:decimal ;  
  sh:name "Frame rate [fps]"@en, "Bildrate [fps]"@de ; 
  qudt:Unit unit:FRAME-PER-SEC .

coscineMicroscopy_RotMagField:rotation
  sh:path obo:PATO_0001599 ;
  sh:order 11 ;
  sh:minCount 1 ;
  sh:datatype xsd:decimal ;  
  sh:name "Rotation [°]"@en, "Drehung [°]"@de ; 
  qudt:Unit unit:DEG .

coscineMicroscopy_RotMagField:microscope
  sh:path obo:CHMO_0000947 ;
  sh:order 12 ;
  sh:minCount 1 ;
  sh:name "Mikroskop"@de, "Microscope"@en ; 
  sh:class <http://purl.org/coscine/vocabularies/sfb985/microscope> .

coscineMicroscopy_RotMagField:objectiveLens
  sh:path repr:Objective ;
  sh:order 13 ;
  sh:minCount 1 ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/objectiveLens> ;
  sh:name "Objective lens"@en, "Objective"@de .

coscineMicroscopy_RotMagField:camera
  sh:path obo:OBI_0001048 ;
  sh:order 14 ;
  sh:minCount 1 ;
  sh:name "Camera"@en, "Kamera"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/camera> .

coscineMicroscopy_RotMagField:comment
  sh:path rdfs:comment ;
  sh:order 15 ;
  sh:datatype xsd:string ; 
  dash:singleLine false ;
  sh:name "Comment"@en, "Kommentar"@de .
