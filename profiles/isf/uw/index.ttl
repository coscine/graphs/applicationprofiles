@base <https://purl.org/coscine/ap/isf/manualUnderwaterArcWelding/> .

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

@prefix dcterms: <http://purl.org/dc/terms/> .

@prefix isf: <https://purl.org/coscine/terms/isf#> .
@prefix coscineISFmanualUnderwaterArcWelding: <https://purl.org/coscine/ap/isf/manualUnderwaterArcWelding#> .

<https://purl.org/coscine/ap/isf/manualUnderwaterArcWelding/>
  dcterms:license <http://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://isf.rwth-aachen.de/> ;
  dcterms:title  "ISF manual underwater arc welding"@en, "ISF E-Hand Unterwasserschweißen"@de ;

  a sh:NodeShape ;
  sh:targetClass <https://purl.org/coscine/ap/isf/manualUnderwaterArcWelding/> ;
  sh:closed true ;

  sh:property [
    sh:path rdf:type ;
  ] ;

  sh:property coscineISFmanualUnderwaterArcWelding:current ;
  sh:property coscineISFmanualUnderwaterArcWelding:voltage ;
  sh:property coscineISFmanualUnderwaterArcWelding:weldingTime ;
  sh:property coscineISFmanualUnderwaterArcWelding:waterTemperature ;
  sh:property coscineISFmanualUnderwaterArcWelding:electrodeName ;
  sh:property coscineISFmanualUnderwaterArcWelding:manufacturer ;
  sh:property coscineISFmanualUnderwaterArcWelding:electrodeDiameter ;
  sh:property coscineISFmanualUnderwaterArcWelding:batch ;
  sh:property coscineISFmanualUnderwaterArcWelding:electrodeInclinationAngle ;
  sh:property coscineISFmanualUnderwaterArcWelding:addtitionalWeightOnElectrodeHolder ;
  sh:property coscineISFmanualUnderwaterArcWelding:workpieceMaterial ;
  sh:property coscineISFmanualUnderwaterArcWelding:weldingPosition ;
  sh:property coscineISFmanualUnderwaterArcWelding:hotStart ;
  sh:property coscineISFmanualUnderwaterArcWelding:arcForce ;
  sh:property coscineISFmanualUnderwaterArcWelding:temperature ;
  sh:property coscineISFmanualUnderwaterArcWelding:currentInductor ;
  sh:property coscineISFmanualUnderwaterArcWelding:voltageInductor .

coscineISFmanualUnderwaterArcWelding:current
  sh:path isf:current ;
  sh:order 0 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Current"@en, "Stromstärke"@de .

coscineISFmanualUnderwaterArcWelding:voltage
  sh:path isf:voltage ;
  sh:order 1 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Voltage"@en, "Spannung"@de .

coscineISFmanualUnderwaterArcWelding:weldingTime
  sh:path isf:weldingTime ;
  sh:order 2 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Welding time"@en, "Schweißzeit"@de .

coscineISFmanualUnderwaterArcWelding:waterTemperature
  sh:path isf:waterTemperature ;
  sh:order 3 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Water temperature"@en, "Wassertemperatur"@de .

coscineISFmanualUnderwaterArcWelding:electrodeName
  sh:path isf:electrodeName ;
  sh:order 4 ;
  sh:minCount 1 ;
  sh:maxCount 2 ;
  sh:datatype xsd:string ;
  sh:name "Elektrode name"@en, "Elektrodenname"@de .

coscineISFmanualUnderwaterArcWelding:manufacturer
  sh:path isf:manufacturer ;
  sh:order 5 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Manufacturer"@en, "Hersteller"@de .

coscineISFmanualUnderwaterArcWelding:electrodeDiameter
  sh:path isf:electrodeDiameter ;
  sh:order 6 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Electrode diameter"@en, "Elektrodendurchmesser"@de .

coscineISFmanualUnderwaterArcWelding:batch
  sh:path isf:batch ;
  sh:order 7 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Batch"@en, "Charge"@de .

coscineISFmanualUnderwaterArcWelding:electrodeInclinationAngle
  sh:path isf:electrodeInclinationAngle ;
  sh:order 8 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Electrode Inclination angle"@en, "Elektrodenanstellwinkel"@de .

coscineISFmanualUnderwaterArcWelding:addtitionalWeightOnElectrodeHolder
  sh:path isf:addtitionalWeightOnElectrodeHolder ;
  sh:order 9 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Additional weight on electrode holder"@en, "zusätzliches Gewicht an Elektrodenhalter"@de .

coscineISFmanualUnderwaterArcWelding:workpieceMaterial
  sh:path isf:workpieceMaterial ;
  sh:order 10 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Base Material"@en, "Grundwerkstoff"@de .

coscineISFmanualUnderwaterArcWelding:weldingPosition
  sh:path isf:weldingPosition ;
  sh:order 11 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Welding position"@en, "Schweißposition"@de .

coscineISFmanualUnderwaterArcWelding:hotStart
  sh:path isf:hotStart ;
  sh:order 12 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Hot start"@en, "Hot-Start"@de .

coscineISFmanualUnderwaterArcWelding:arcForce
  sh:path isf:arcForce ;
  sh:order 13 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Arc force"@en, "Arc-Force"@de .

coscineISFmanualUnderwaterArcWelding:temperature
  sh:path isf:temperature ;
  sh:order 14 ;
  sh:minCount 8 ;
  sh:maxCount 8 ;
  sh:datatype xsd:string ;
  sh:name "Temperature"@en, "Temperatur"@de .

coscineISFmanualUnderwaterArcWelding:currentInductor
  sh:path isf:currentInductor ;
  sh:order 15 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Current inductor"@en, "Strom Induktor"@de .

coscineISFmanualUnderwaterArcWelding:voltageInductor
  sh:path isf:voltageInductor ;
  sh:order 16 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Voltage inductor"@en, "Spannung Induktor"@de .
