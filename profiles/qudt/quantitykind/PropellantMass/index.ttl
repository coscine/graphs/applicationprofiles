@base <https://purl.org/coscine/ap/qudt/quantitykind/PropellantMass/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/PropellantMass/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Propellant Mass sind."@de,
    "Profile for all units that are a/an Propellant Mass."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Propellant Mass"@de,
    "Unit: Propellant Mass"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:AMU unit:CARAT unit:CWT_LONG unit:CWT_SHORT unit:CentiGM unit:DRAM_UK unit:DRAM_US unit:DWT unit:DecaGM unit:DeciGM unit:DeciTONNE unit:DeciTON_Metric unit:EarthMass unit:FemtoGM unit:GM unit:GRAIN unit:HectoGM unit:Hundredweight_UK unit:Hundredweight_US unit:KiloGM unit:KiloTONNE unit:KiloTON_Metric unit:LB unit:LB_M unit:LB_T unit:LunarMass unit:MegaGM unit:MicroGM unit:MilliGM unit:NanoGM unit:OZ unit:OZ_M unit:OZ_TROY unit:Pennyweight unit:PicoGM unit:PlanckMass unit:Quarter_UK unit:SLUG unit:SolarMass unit:Stone_UK unit:TONNE unit:TON_Assay unit:TON_LONG unit:TON_Metric unit:TON_SHORT unit:TON_UK unit:TON_US unit:U ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:AMU rdfs:label "Atomic Mass Unit"@en .
unit:CARAT rdfs:label "Carat"@en .
unit:CWT_LONG rdfs:label "Long Hundred Weight"@en .
unit:CWT_SHORT rdfs:label "Hundred Weight - Short"@en .
unit:CentiGM rdfs:label "Centigram"@en .
unit:DRAM_UK rdfs:label "Dram (Uk)"@en .
unit:DRAM_US rdfs:label "Dram (Us)"@en .
unit:DWT rdfs:label "Penny Weight"@en .
unit:DecaGM rdfs:label "Decagram"@en .
unit:DeciGM rdfs:label "Decigram"@en .
unit:DeciTONNE rdfs:label "Decitonne"@en .
unit:DeciTON_Metric rdfs:label "Metric Deciton"@en .
unit:EarthMass rdfs:label "Earth Mass"@en .
unit:FemtoGM rdfs:label "Femtogram"@en .
unit:GM rdfs:label "Gram"@en .
unit:GRAIN rdfs:label "Grain"@en .
unit:HectoGM rdfs:label "Hectogram"@en .
unit:Hundredweight_UK rdfs:label "Hundredweight (Uk)"@en .
unit:Hundredweight_US rdfs:label "Hundredweight (Us)"@en .
unit:KiloGM rdfs:label "Kilogramm"@de, "Chiliogramma"@la, "Chilogrammo"@it, "Kilogram"@cs, "Kilogram"@en, "Kilogram"@ms, "Kilogram"@pl, "Kilogram"@ro, "Kilogram"@sl, "Kilogram"@tr, "Kilogramm*"@hu, "Kilogramme"@fr, "Kilogramo"@es, "Quilograma"@pt, "Χιλιόγραμμο"@el, "Килограм"@bg, "Килограмм"@ru, "קילוגרם"@he, "كيلوغرام"@ar, "کیلوگرم"@fa, "किलोग्राम"@hi, "キログラム"@ja, "公斤"@zh .
unit:KiloTONNE rdfs:label "Kilotonne"@en .
unit:KiloTON_Metric rdfs:label "Metric Kiloton"@en .
unit:LB rdfs:label "Pound Mass"@en .
unit:LB_M rdfs:label "Pound Mass"@en .
unit:LB_T rdfs:label "Pound Troy"@en .
unit:LunarMass rdfs:label "Lunar Mass"@en .
unit:MegaGM rdfs:label "Megagram"@en .
unit:MicroGM rdfs:label "Microgram"@en .
unit:MilliGM rdfs:label "Milligram"@en .
unit:NanoGM rdfs:label "Nanograms"@en .
unit:OZ rdfs:label "Ounce Mass"@en .
unit:OZ_M rdfs:label "Ounce Mass"@en .
unit:OZ_TROY rdfs:label "Ounce Troy"@en .
unit:Pennyweight rdfs:label "Pennyweight"@en .
unit:PicoGM rdfs:label "Picograms"@en .
unit:PlanckMass rdfs:label "Planck Mass"@en .
unit:Quarter_UK rdfs:label "Quarter (Uk)"@en .
unit:SLUG rdfs:label "Slug"@en .
unit:SolarMass rdfs:label "Solar Mass"@en .
unit:Stone_UK rdfs:label "Stone (Uk)"@en .
unit:TONNE rdfs:label "Tonne"@en .
unit:TON_Assay rdfs:label "Assay Ton"@en .
unit:TON_LONG rdfs:label "Long Ton"@en .
unit:TON_Metric rdfs:label "Metric Ton"@en .
unit:TON_SHORT rdfs:label "Short Ton"@en .
unit:TON_UK rdfs:label "Ton (Uk)"@en .
unit:TON_US rdfs:label "Ton (Us)"@en .
unit:U rdfs:label "Unified Atomic Mass Unit"@en .
