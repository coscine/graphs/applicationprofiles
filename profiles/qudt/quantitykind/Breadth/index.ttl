@base <https://purl.org/coscine/ap/qudt/quantitykind/Breadth/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/Breadth/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Breite sind."@de,
    "Profile for all units that are a/an Breadth."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Breite"@de,
    "Unit: Breadth"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:ANGSTROM unit:AU unit:BTU_IT-PER-LB_F unit:CH unit:CentiM unit:DecaM unit:DeciM unit:FATH unit:FM unit:FT unit:FT_US unit:FUR unit:FUR_Long unit:FemtoM unit:GAUGE_FR unit:HectoM unit:IN unit:KiloM unit:LY unit:M unit:MI unit:MI_N unit:MI_US unit:MicroIN unit:MicroM unit:MilLength unit:MilliIN unit:MilliM unit:NanoM unit:PARSEC unit:PCA unit:PT unit:PicoM unit:PlanckLength unit:ROD unit:YD ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:ANGSTROM rdfs:label "Angstrom"@en .
unit:AU rdfs:label "Astronomical-Unit"@en .
unit:BTU_IT-PER-LB_F rdfs:label "British Thermal Unit (International Table) Per Pound Of Force"@en .
unit:CH rdfs:label "Chain"@en .
unit:CentiM rdfs:label "Centimeter"@en-us, "Centimetre"@en .
unit:DecaM rdfs:label "Decameter"@en-us, "Decametre"@en .
unit:DeciM rdfs:label "Decimeter"@en-us, "Decimetre"@en .
unit:FATH rdfs:label "Fathom"@en .
unit:FM rdfs:label "Fermi"@en .
unit:FT rdfs:label "Foot"@en .
unit:FT_US rdfs:label "Us Survey Foot"@en .
unit:FUR rdfs:label "Furlong"@en .
unit:FUR_Long rdfs:label "Long Furlong"@en .
unit:FemtoM rdfs:label "Femtometer"@en-us, "Femtometre"@en .
unit:GAUGE_FR rdfs:label "French Gauge"@en .
unit:HectoM rdfs:label "Hectometer"@en-us, "Hectometre"@en .
unit:IN rdfs:label "Inch"@en .
unit:KiloM rdfs:label "Kilometer"@en-us, "Kilometre"@en .
unit:LY rdfs:label "Light Year"@en .
unit:M rdfs:label "Meter"@de, "Meter"@en-us, "Meter"@ms, "Meter"@sl, "Metr"@cs, "Metr"@pl, "Metre"@en, "Metre"@tr, "Metro"@es, "Metro"@it, "Metro"@pt, "Metru"@ro, "Metrum"@la, "Mètre"@fr, "Méter"@hu, "Μέτρο"@el, "Метр"@ru, "Метър"@bg, "מטר"@he, "متر"@ar, "متر"@fa, "मीटर"@hi, "メートル"@ja, "米"@zh .
unit:MI rdfs:label "International Mile"@en .
unit:MI_N rdfs:label "Nautical Mile"@en .
unit:MI_US rdfs:label "Mile Us Statute"@en .
unit:MicroIN rdfs:label "Microinch"@en .
unit:MicroM rdfs:label "Micrometer"@en-us, "Micrometre"@en .
unit:MilLength rdfs:label "Mil Length"@en .
unit:MilliIN rdfs:label "Milli-Inch"@en .
unit:MilliM rdfs:label "Millimeter"@en-us, "Millimetre"@en .
unit:NanoM rdfs:label "Nanometer"@en-us, "Nanometre"@en .
unit:PARSEC rdfs:label "Parsec"@en .
unit:PCA rdfs:label "Pica"@en .
unit:PT rdfs:label "Point"@en .
unit:PicoM rdfs:label "Picometer"@en-us, "Picometre"@en .
unit:PlanckLength rdfs:label "Planck Length"@en .
unit:ROD rdfs:label "Rod"@en .
unit:YD rdfs:label "Yard"@en .
