@base <https://purl.org/coscine/ap/qudt/quantitykind/TotalPressure/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/TotalPressure/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Total Pressure sind."@de,
    "Profile for all units that are a/an Total Pressure."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Total Pressure"@de,
    "Unit: Total Pressure"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:ATM unit:ATM_T unit:BAR unit:BARAD unit:BARYE unit:CentiBAR unit:CentiM_H2O unit:CentiM_HG unit:DYN-PER-CentiM2 unit:DecaPA unit:DeciBAR unit:FT_H2O unit:FT_HG unit:GM_F-PER-CentiM2 unit:GigaPA unit:HectoBAR unit:HectoPA unit:IN_H2O unit:IN_HG unit:KIP_F-PER-IN2 unit:KiloBAR unit:KiloGM-PER-M-SEC2 unit:KiloGM_F-PER-CentiM2 unit:KiloGM_F-PER-M2 unit:KiloGM_F-PER-MilliM2 unit:KiloLB_F-PER-IN2 unit:KiloPA unit:KiloPA_A unit:LB_F-PER-FT2 unit:LB_F-PER-IN2 unit:MegaBAR unit:MegaPA unit:MegaPSI unit:MicroATM unit:MicroBAR unit:MicroPA unit:MicroTORR unit:MilliBAR unit:MilliM_H2O unit:MilliM_HG unit:MilliM_HGA unit:MilliPA unit:MilliTORR unit:N-PER-CentiM2 unit:N-PER-M2 unit:N-PER-MilliM2 unit:PA unit:PDL-PER-FT2 unit:PSI unit:PicoPA unit:PlanckPressure unit:TORR ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:ATM rdfs:label "Standard Atmosphere"@en .
unit:ATM_T rdfs:label "Technical Atmosphere"@en .
unit:BAR rdfs:label "Bar"@en .
unit:BARAD rdfs:label "Barad"@en .
unit:BARYE rdfs:label "Barye"@en .
unit:CentiBAR rdfs:label "Centibar"@en .
unit:CentiM_H2O rdfs:label "Conventional Centimeter Of Water"@en-us, "Conventional Centimetre Of Water"@en .
unit:CentiM_HG rdfs:label "Centimeter Of Mercury"@en-us, "Centimetre Of Mercury"@en .
unit:DYN-PER-CentiM2 rdfs:label "Dyne Per Square Centimeter"@en-us, "Dyne Per Square Centimetre"@en .
unit:DecaPA rdfs:label "Decapascal"@en .
unit:DeciBAR rdfs:label "Decibar"@en .
unit:FT_H2O rdfs:label "Foot Of Water"@en .
unit:FT_HG rdfs:label "Foot Of Mercury"@en .
unit:GM_F-PER-CentiM2 rdfs:label "Gram Force Per Square Centimeter"@en-us, "Gram Force Per Square Centimetre"@en .
unit:GigaPA rdfs:label "Gigapascal"@en .
unit:HectoBAR rdfs:label "Hectobar"@en .
unit:HectoPA rdfs:label "Hectopascal"@en .
unit:IN_H2O rdfs:label "Inch Of Water"@en .
unit:IN_HG rdfs:label "Inch Of Mercury"@en .
unit:KIP_F-PER-IN2 rdfs:label "Kip Per Square Inch"@en .
unit:KiloBAR rdfs:label "Kilobar"@en .
unit:KiloGM-PER-M-SEC2 rdfs:label "Kilograms Per Metre Per Square Second"@en .
unit:KiloGM_F-PER-CentiM2 rdfs:label "Kilogram Force Per Square Centimeter"@en-us, "Kilogram Force Per Square Centimetre"@en .
unit:KiloGM_F-PER-M2 rdfs:label "Kilogram Force Per Square Meter"@en-us, "Kilogram Force Per Square Metre"@en .
unit:KiloGM_F-PER-MilliM2 rdfs:label "Kilogram Force Per Square Millimeter"@en-us, "Kilogram Force Per Square Millimetre"@en .
unit:KiloLB_F-PER-IN2 rdfs:label "Kilopound Force Per Square Inch"@en .
unit:KiloPA rdfs:label "Kilopascal"@en .
unit:KiloPA_A rdfs:label "Kilopascal Absolute"@en .
unit:LB_F-PER-FT2 rdfs:label "Pound Force Per Square Foot"@en .
unit:LB_F-PER-IN2 rdfs:label "Pound Force Per Square Inch"@en .
unit:MegaBAR rdfs:label "Megabar"@en .
unit:MegaPA rdfs:label "Megapascal"@en .
unit:MegaPSI rdfs:label "Mpsi"@en .
unit:MicroATM rdfs:label "Microatmospheres"@en .
unit:MicroBAR rdfs:label "Microbar"@en .
unit:MicroPA rdfs:label "Micropascal"@en .
unit:MicroTORR rdfs:label "Microtorr"@en .
unit:MilliBAR rdfs:label "Millibar"@en .
unit:MilliM_H2O rdfs:label "Conventional Millimeter Of Water"@en-us, "Conventional Millimetre Of Water"@en .
unit:MilliM_HG rdfs:label "Millimeter Of Mercury"@en-us, "Millimetre Of Mercury"@en .
unit:MilliM_HGA rdfs:label "Millimeter Of Mercury - Absolute"@en-us, "Millimetre Of Mercury - Absolute"@en .
unit:MilliPA rdfs:label "Millipascal"@en .
unit:MilliTORR rdfs:label "Millitorr"@en .
unit:N-PER-CentiM2 rdfs:label "Newton Per Square Centimeter"@en-us, "Newton Per Square Centimetre"@en .
unit:N-PER-M2 rdfs:label "Newtons Per Square Meter"@en-us, "Newtons Per Square Metre"@en .
unit:N-PER-MilliM2 rdfs:label "Newton Per Square Millimeter"@en-us, "Newton Per Square Millimetre"@en .
unit:PA rdfs:label "Pascal"@de, "Pascal"@cs, "Pascal"@en, "Pascal"@es, "Pascal"@fr, "Pascal"@hu, "Pascal"@it, "Pascal"@ms, "Pascal"@pt, "Pascal"@ro, "Pascal"@sl, "Pascal"@tr, "Pascalium"@la, "Paskal"@pl, "Πασκάλ"@el, "Паскал"@bg, "Паскаль"@ru, "פסקל"@he, "باسكال"@ar, "پاسگال"@fa, "पास्कल"@hi, "パスカル"@ja, "帕斯卡"@zh .
unit:PDL-PER-FT2 rdfs:label "Poundal Per Square Foot"@en .
unit:PSI rdfs:label "Psi"@en .
unit:PicoPA rdfs:label "Picopascal"@en .
unit:PlanckPressure rdfs:label "Planck Pressure"@en .
unit:TORR rdfs:label "Torr"@en .
