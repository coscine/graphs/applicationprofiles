@base <https://purl.org/coscine/ap/qudt/quantitykind/InstantaneousPower/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/InstantaneousPower/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Instantaneous Power sind."@de,
    "Profile for all units that are a/an Instantaneous Power."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Instantaneous Power"@de,
    "Unit: Instantaneous Power"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:BAR-L-PER-SEC unit:BAR-M3-PER-SEC unit:BTU_IT-PER-HR unit:BTU_IT-PER-SEC unit:ERG-PER-SEC unit:FT-LB_F-PER-HR unit:FT-LB_F-PER-MIN unit:FT-LB_F-PER-SEC unit:GigaJ-PER-HR unit:GigaW unit:HP unit:HP_Boiler unit:HP_Brake unit:HP_Electric unit:HP_Metric unit:J-PER-HR unit:J-PER-SEC unit:KiloBTU_IT-PER-HR unit:KiloCAL-PER-MIN unit:KiloCAL-PER-SEC unit:KiloW unit:MegaBTU_IT-PER-HR unit:MegaJ-PER-HR unit:MegaJ-PER-SEC unit:MegaPA-L-PER-SEC unit:MegaPA-M3-PER-SEC unit:MegaW unit:MicroW unit:MilliBAR-L-PER-SEC unit:MilliBAR-M3-PER-SEC unit:MilliW unit:NanoW unit:PA-L-PER-SEC unit:PA-M3-PER-SEC unit:PSI-IN3-PER-SEC unit:PSI-M3-PER-SEC unit:PSI-YD3-PER-SEC unit:PicoW unit:PlanckPower unit:THM_US-PER-HR unit:TON_FG unit:TeraW unit:W ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:BAR-L-PER-SEC rdfs:label "Bar Liter Per Second"@en-us, "Bar Litre Per Second"@en .
unit:BAR-M3-PER-SEC rdfs:label "Bar Cubic Meter Per Second"@en-us, "Bar Cubic Metre Per Second"@en .
unit:BTU_IT-PER-HR rdfs:label "Btu Per Hour"@en .
unit:BTU_IT-PER-SEC rdfs:label "Btu Per Second"@en .
unit:ERG-PER-SEC rdfs:label "Erg Per Second"@en .
unit:FT-LB_F-PER-HR rdfs:label "Foot Pound Force Per Hour"@en .
unit:FT-LB_F-PER-MIN rdfs:label "Foot Pound Force Per Minute"@en .
unit:FT-LB_F-PER-SEC rdfs:label "Foot Pound Force Per Second"@en .
unit:GigaJ-PER-HR rdfs:label "Gigajoule Per Hour"@en .
unit:GigaW rdfs:label "Gigawatt"@en .
unit:HP rdfs:label "Horsepower"@en .
unit:HP_Boiler rdfs:label "Boiler Horsepower"@en .
unit:HP_Brake rdfs:label "Horsepower (Brake)"@en .
unit:HP_Electric rdfs:label "Horsepower (Electric)"@en .
unit:HP_Metric rdfs:label "Horsepower (Metric)"@en .
unit:J-PER-HR rdfs:label "Joule Per Hour"@en .
unit:J-PER-SEC rdfs:label "Joule Per Second"@en .
unit:KiloBTU_IT-PER-HR rdfs:label "Kilo British Thermal Unit (International Definition) Per Hour"@en .
unit:KiloCAL-PER-MIN rdfs:label "Kilocalorie Per Minute"@en .
unit:KiloCAL-PER-SEC rdfs:label "Kilocalorie Per Second"@en .
unit:KiloW rdfs:label "Kilowatt"@en .
unit:MegaBTU_IT-PER-HR rdfs:label "Mega British Thermal Unit (International Definition) Per Hour"@en .
unit:MegaJ-PER-HR rdfs:label "Megajoule Per Hour"@en .
unit:MegaJ-PER-SEC rdfs:label "Megajoule Per Second"@en .
unit:MegaPA-L-PER-SEC rdfs:label "Megapascal Liter Per Second"@en-us, "Megapascal Litre Per Second"@en .
unit:MegaPA-M3-PER-SEC rdfs:label "Megapascal Cubic Meter Per Second"@en-us, "Megapascal Cubic Metre Per Second"@en .
unit:MegaW rdfs:label "Megaw"@en .
unit:MicroW rdfs:label "Microwatt"@en .
unit:MilliBAR-L-PER-SEC rdfs:label "Millibar Liter Per Second"@en-us, "Millibar Litre Per Second"@en .
unit:MilliBAR-M3-PER-SEC rdfs:label "Millibar Cubic Meter Per Second"@en-us, "Millibar Cubic Metre Per Second"@en .
unit:MilliW rdfs:label "Milliw"@en .
unit:NanoW rdfs:label "Nanowatt"@en .
unit:PA-L-PER-SEC rdfs:label "Pascal Liter Per Second"@en-us, "Pascal Litre Per Second"@en .
unit:PA-M3-PER-SEC rdfs:label "Pascal Cubic Meter Per Second"@en-us, "Pascal Cubic Metre Per Second"@en .
unit:PSI-IN3-PER-SEC rdfs:label "Psi Cubic Inch Per Second"@en .
unit:PSI-M3-PER-SEC rdfs:label "Psi Cubic Meter Per Second"@en-us, "Psi Cubic Metre Per Second"@en .
unit:PSI-YD3-PER-SEC rdfs:label "Psi Cubic Yard Per Second"@en .
unit:PicoW rdfs:label "Picowatt"@en .
unit:PlanckPower rdfs:label "Planck Power"@en .
unit:THM_US-PER-HR rdfs:label "Therm Us Per Hour"@en .
unit:TON_FG rdfs:label "Ton Of Refrigeration"@en .
unit:TeraW rdfs:label "Terawatt"@en .
unit:W rdfs:label "Watt"@de, "Vatio"@es, "Wat"@pl, "Watt"@cs, "Watt"@en, "Watt"@fr, "Watt"@hu, "Watt"@it, "Watt"@ms, "Watt"@pt, "Watt"@ro, "Watt"@sl, "Watt"@tr, "Wattium"@la, "Βατ"@el, "Ват"@bg, "Ватт"@ru, "ואט"@he, "وات"@fa, "واط"@ar, "वाट"@hi, "ワット"@ja, "瓦特"@zh .
