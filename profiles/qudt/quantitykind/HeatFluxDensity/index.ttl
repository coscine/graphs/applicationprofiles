@base <https://purl.org/coscine/ap/qudt/quantitykind/HeatFluxDensity/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/HeatFluxDensity/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Heat Flux Density sind."@de,
    "Profile for all units that are a/an Heat Flux Density."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Heat Flux Density"@de,
    "Unit: Heat Flux Density"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:BTU_IT-PER-HR-FT2 unit:BTU_IT-PER-SEC-FT2 unit:ERG-PER-CentiM2-SEC unit:FT-LB_F-PER-FT2-SEC unit:J-PER-CentiM2-DAY unit:KiloCAL-PER-CentiM2-MIN unit:KiloCAL-PER-CentiM2-SEC unit:MicroW-PER-M2 unit:MilliW-PER-M2 unit:PSI-L-PER-SEC unit:PicoW-PER-M2 unit:W-PER-CentiM2 unit:W-PER-FT2 unit:W-PER-IN2 unit:W-PER-M2 ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:BTU_IT-PER-HR-FT2 rdfs:label "Btu Per Hour Square Foot"@en .
unit:BTU_IT-PER-SEC-FT2 rdfs:label "Btu Per Second Square Foot"@en .
unit:ERG-PER-CentiM2-SEC rdfs:label "Erg Per Square Centimeter Second"@en-us, "Erg Per Square Centimetre Second"@en .
unit:FT-LB_F-PER-FT2-SEC rdfs:label "Foot Pound Force Per Square Foot Second"@en .
unit:J-PER-CentiM2-DAY rdfs:label "Joules Per Square Centimetre Per Day"@en .
unit:KiloCAL-PER-CentiM2-MIN rdfs:label "Kilocalorie Per Square Centimeter Minute"@en-us, "Kilocalorie Per Square Centimetre Minute"@en .
unit:KiloCAL-PER-CentiM2-SEC rdfs:label "Kilocalorie Per Square Centimeter Second"@en-us, "Kilocalorie Per Square Centimetre Second"@en .
unit:MicroW-PER-M2 rdfs:label "Microwatt Per Square Meter"@en-us, "Microwatt Per Square Metre"@en .
unit:MilliW-PER-M2 rdfs:label "Milliwatt Per Square Meter"@en-us, "Milliwatt Per Square Metre"@en .
unit:PSI-L-PER-SEC rdfs:label "Psi Liter Per Second"@en-us, "Psi Litre Per Second"@en .
unit:PicoW-PER-M2 rdfs:label "Picowatt Per Square Meter"@en-us, "Picowatt Per Square Metre"@en .
unit:W-PER-CentiM2 rdfs:label "Watt Per Square Centimeter"@en-us, "Watt Per Square Centimetre"@en .
unit:W-PER-FT2 rdfs:label "Watt Per Square Foot"@en .
unit:W-PER-IN2 rdfs:label "Watt Per Square Inch"@en .
unit:W-PER-M2 rdfs:label "Watt Je Quadratmeter"@de, "Watt Per Square Meter"@en-us, "Vatio Por Metro Cuadrado"@es, "Wat Na Metr Kwadratowy"@pl, "Watt Al Metro Quadrato"@it, "Watt Bölü Metre Kare"@tr, "Watt Na Kvadratni Meter"@sl, "Watt Na Metr Čtvereční"@cs, "Watt Par Mètre Carré"@fr, "Watt Pe Metru Pătrat"@ro, "Watt Per Meter Persegi"@ms, "Watt Per Square Metre"@en, "Watt Por Metro Quadrado"@pt, "Ватт На Квадратный Метр"@ru, "وات بر مترمربع"@fa, "واط في المتر المربع"@ar, "वाट प्रति वर्ग मीटर"@hi, "ワット毎平方メートル"@ja, "瓦特每平方米"@zh .
