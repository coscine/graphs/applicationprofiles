@base <https://purl.org/coscine/ap/qudt/quantitykind/ReverberationTime/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/ReverberationTime/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Reverberation Time sind."@de,
    "Profile for all units that are a/an Reverberation Time."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Reverberation Time"@de,
    "Unit: Reverberation Time"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:CentiPOISE-PER-BAR unit:DAY unit:DAY_Sidereal unit:H-PER-KiloOHM unit:H-PER-OHM unit:HR unit:HR_Sidereal unit:KiloSEC unit:KiloYR unit:MIN unit:MIN_Sidereal unit:MO unit:MO_MeanGREGORIAN unit:MO_MeanJulian unit:MO_Synodic unit:MegaYR unit:MicroH-PER-KiloOHM unit:MicroH-PER-OHM unit:MicroSEC unit:MilliH-PER-KiloOHM unit:MilliH-PER-OHM unit:MilliPA-SEC-PER-BAR unit:MilliSEC unit:NanoSEC unit:PA-SEC-PER-BAR unit:POISE-PER-BAR unit:PicoSEC unit:PlanckTime unit:SEC unit:SH unit:WK unit:YR unit:YR_Common unit:YR_Sidereal unit:YR_TROPICAL ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:CentiPOISE-PER-BAR rdfs:label "Centipoise Per Bar"@en .
unit:DAY rdfs:label "Day"@en .
unit:DAY_Sidereal rdfs:label "Sidereal Day"@en .
unit:H-PER-KiloOHM rdfs:label "Henry Per Kiloohm"@en .
unit:H-PER-OHM rdfs:label "Henry Per Ohm"@en .
unit:HR rdfs:label "Hour"@en .
unit:HR_Sidereal rdfs:label "Sidereal Hour"@en .
unit:KiloSEC rdfs:label "Kilosecond"@en .
unit:KiloYR rdfs:label "Kiloyear"@en .
unit:MIN rdfs:label "Minute"@en .
unit:MIN_Sidereal rdfs:label "Sidereal Minute"@en .
unit:MO rdfs:label "Month"@en .
unit:MO_MeanGREGORIAN rdfs:label "Mean Gregorian Month"@en .
unit:MO_MeanJulian rdfs:label "Mean Julian Month"@en .
unit:MO_Synodic rdfs:label "Synodic Month"@en .
unit:MegaYR rdfs:label "Million Years"@en .
unit:MicroH-PER-KiloOHM rdfs:label "Microhenry Per Kiloohm"@en .
unit:MicroH-PER-OHM rdfs:label "Microhenry Per Ohm"@en .
unit:MicroSEC rdfs:label "Microsecond"@en .
unit:MilliH-PER-KiloOHM rdfs:label "Millihenry Per Kiloohm"@en .
unit:MilliH-PER-OHM rdfs:label "Millihenry Per Ohm"@en .
unit:MilliPA-SEC-PER-BAR rdfs:label "Millipascal Second Per Bar"@en .
unit:MilliSEC rdfs:label "Millisecond"@en .
unit:NanoSEC rdfs:label "Nanosecond"@en .
unit:PA-SEC-PER-BAR rdfs:label "Pascal Second Per Bar"@en .
unit:POISE-PER-BAR rdfs:label "Poise Per Bar"@en .
unit:PicoSEC rdfs:label "Picosecond"@en .
unit:PlanckTime rdfs:label "Planck Time"@en .
unit:SEC rdfs:label "Sekunde"@de, "Másodperc"@hu, "Saat"@ms, "Saniye"@tr, "Second"@en, "Seconde"@fr, "Secondo"@it, "Secundum"@la, "Secundă"@ro, "Segundo"@es, "Segundo"@pt, "Sekunda"@cs, "Sekunda"@pl, "Sekunda"@sl, "Δευτερόλεπτο"@el, "Секунда"@bg, "Секунда"@ru, "שנייה"@he, "ثانية"@ar, "ثانیه"@fa, "सैकण्ड"@hi, "秒"@ja, "秒"@zh .
unit:SH rdfs:label "Shake"@en .
unit:WK rdfs:label "Week"@en .
unit:YR rdfs:label "Year"@en .
unit:YR_Common rdfs:label "Common Year"@en .
unit:YR_Sidereal rdfs:label "Sidereal Year"@en .
unit:YR_TROPICAL rdfs:label "Tropical Year"@en .
