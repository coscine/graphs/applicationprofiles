@base <https://purl.org/coscine/ap/qudt/quantitykind/ThermalConductivity/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/ThermalConductivity/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Thermal Conductivity sind."@de,
    "Profile for all units that are a/an Thermal Conductivity."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Thermal Conductivity"@de,
    "Unit: Thermal Conductivity"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:BTU_IT-FT-PER-FT2-HR-DEG_F unit:BTU_IT-IN-PER-FT2-HR-DEG_F unit:BTU_IT-IN-PER-FT2-SEC-DEG_F unit:BTU_IT-IN-PER-HR-FT2-DEG_F unit:BTU_IT-IN-PER-SEC-FT2-DEG_F unit:BTU_IT-PER-SEC-FT-DEG_R unit:BTU_TH-FT-PER-FT2-HR-DEG_F unit:BTU_TH-FT-PER-HR-FT2-DEG_F unit:BTU_TH-IN-PER-FT2-HR-DEG_F unit:BTU_TH-IN-PER-FT2-SEC-DEG_F unit:CAL_IT-PER-SEC-CentiM-K unit:CAL_TH-PER-CentiM-SEC-DEG_C unit:CAL_TH-PER-SEC-CentiM-K unit:KiloCAL-PER-CentiM-SEC-DEG_C unit:KiloCAL_IT-PER-HR-M-DEG_C unit:W-PER-M-K ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:BTU_IT-FT-PER-FT2-HR-DEG_F rdfs:label "Btu (It) Foot Per Square Foot Hour Degree Fahrenheit"@en .
unit:BTU_IT-IN-PER-FT2-HR-DEG_F rdfs:label "British Thermal Unit (International Table) Inch Per Hour Square Foot Degree Fahrenheit"@en .
unit:BTU_IT-IN-PER-FT2-SEC-DEG_F rdfs:label "Btu (It) Inch Per Square Foot Second Degree Fahrenheit"@en .
unit:BTU_IT-IN-PER-HR-FT2-DEG_F rdfs:label "British Thermal Unit (International Table) Inch Per Hour Square Foot Degree Fahrenheit"@en .
unit:BTU_IT-IN-PER-SEC-FT2-DEG_F rdfs:label "British Thermal Unit (International Table) Inch Per Second Square Foot Degree Fahrenheit"@en .
unit:BTU_IT-PER-SEC-FT-DEG_R rdfs:label "British Thermal Unit (International Table) Per Second Foot Degree Rankine"@en .
unit:BTU_TH-FT-PER-FT2-HR-DEG_F rdfs:label "Btu (Th) Foot Per Square Foot Hour Degree Fahrenheit"@en .
unit:BTU_TH-FT-PER-HR-FT2-DEG_F rdfs:label "British Thermal Unit (Thermochemical) Foot Per Hour Square Foot Degree Fahrenheit"@en .
unit:BTU_TH-IN-PER-FT2-HR-DEG_F rdfs:label "Btu (Th) Inch Per Square Foot Hour Degree Fahrenheit"@en .
unit:BTU_TH-IN-PER-FT2-SEC-DEG_F rdfs:label "Btu (Th) Inch Per Square Foot Second Degree Fahrenheit"@en .
unit:CAL_IT-PER-SEC-CentiM-K rdfs:label "Calorie (International Table) Per Second Centimeter Kelvin"@en-us, "Calorie (International Table) Per Second Centimetre Kelvin"@en .
unit:CAL_TH-PER-CentiM-SEC-DEG_C rdfs:label "Calorie (Thermochemical) Per Centimeter Second Degree Celsius"@en-us, "Calorie (Thermochemical) Per Centimetre Second Degree Celsius"@en .
unit:CAL_TH-PER-SEC-CentiM-K rdfs:label "Calorie (Thermochemical) Per Second Centimeter Kelvin"@en-us, "Calorie (Thermochemical) Per Second Centimetre Kelvin"@en .
unit:KiloCAL-PER-CentiM-SEC-DEG_C rdfs:label "Kilocalorie Per Centimeter Second Degree Celsius"@en-us, "Kilocalorie Per Centimetre Second Degree Celsius"@en .
unit:KiloCAL_IT-PER-HR-M-DEG_C rdfs:label "Kilocalorie (International Table) Per Hour Meter Degree Celsius"@en-us, "Kilocalorie (International Table) Per Hour Metre Degree Celsius"@en .
unit:W-PER-M-K rdfs:label "Watt Per Meter Kelvin"@en-us, "Watt Per Metre Kelvin"@en .
