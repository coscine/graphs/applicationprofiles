@base <https://purl.org/coscine/ap/qudt/quantitykind/AbsoluteHumidity/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/AbsoluteHumidity/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Absolute Humidity sind."@de,
    "Profile for all units that are a/an Absolute Humidity."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Absolute Humidity"@de,
    "Unit: Absolute Humidity"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:GRAIN-PER-GAL unit:KiloGM-PER-M3 unit:LB-PER-FT3 unit:LB-PER-GAL unit:LB-PER-GAL_UK unit:LB-PER-GAL_US unit:LB-PER-IN3 unit:LB-PER-M3 unit:LB-PER-YD3 unit:MilliGM-PER-DeciL unit:OZ-PER-GAL unit:OZ-PER-IN3 unit:PlanckDensity unit:SLUG-PER-FT3 unit:TON_LONG-PER-YD3 unit:TON_SHORT-PER-YD3 ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:GRAIN-PER-GAL rdfs:label "Grain Per Gallon"@en .
unit:KiloGM-PER-M3 rdfs:label "Kilogramm Je Kubikmeter"@de, "Chilogrammo Al Metro Cubo"@it, "Kilogram Bölü Metre Küp"@tr, "Kilogram Na Kubični Meter"@sl, "Kilogram Na Metr Krychlový"@cs, "Kilogram Na Metr Sześcienny"@pl, "Kilogram Pe Metru Cub"@ro, "Kilogram Per Cubic Meter"@en-us, "Kilogram Per Cubic Metre"@en, "Kilogram Per Meter Kubik"@ms, "Kilogramme Par Mètre Cube"@fr, "Kilogramo Por Metro Cúbico"@es, "Quilograma Por Metro Cúbico"@pt, "Χιλιόγραμμο Ανά Κυβικό Μέτρο"@el, "Килограм На Кубичен Метър"@bg, "Килограмм На Кубический Метр"@ru, "كيلوغرام لكل متر مكعب"@ar, "کیلوگرم بر متر مکعب"@fa, "किलोग्राम प्रति घन मीटर"@hi, "キログラム毎立方メートル"@ja, "千克每立方米"@zh .
unit:LB-PER-FT3 rdfs:label "Pound Per Cubic Foot"@en .
unit:LB-PER-GAL rdfs:label "Pound Per Gallon"@en .
unit:LB-PER-GAL_UK rdfs:label "Pound (Avoirdupois) Per Gallon (Uk)"@en .
unit:LB-PER-GAL_US rdfs:label "Pound (Avoirdupois) Per Gallon (Us)"@en .
unit:LB-PER-IN3 rdfs:label "Pound Per Cubic Inch"@en .
unit:LB-PER-M3 rdfs:label "Pound Per Cubic Meter"@en-us, "Pound Per Cubic Metre"@en .
unit:LB-PER-YD3 rdfs:label "Pound Per Cubic Yard"@en .
unit:MilliGM-PER-DeciL rdfs:label "Milligrams Per Decilitre"@en, "Milligrams Per Decilitre"@en-us .
unit:OZ-PER-GAL rdfs:label "Imperial Mass Ounce Per Gallon"@en .
unit:OZ-PER-IN3 rdfs:label "Imperial Mass Ounce Per Cubic Inch"@en .
unit:PlanckDensity rdfs:label "Planck Density"@en .
unit:SLUG-PER-FT3 rdfs:label "Slug Per Cubic Foot"@en .
unit:TON_LONG-PER-YD3 rdfs:label "Long Ton Per Cubic Yard"@en .
unit:TON_SHORT-PER-YD3 rdfs:label "Short Ton Per Cubic Yard"@en .
