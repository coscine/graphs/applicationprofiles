@base <https://purl.org/coscine/ap/qudt/quantitykind/FirstMomentOfArea/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/FirstMomentOfArea/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine First Moment Of Area sind."@de,
    "Profile for all units that are a/an First Moment Of Area."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: First Moment Of Area"@de,
    "Unit: First Moment Of Area"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:AC-FT unit:ANGSTROM3 unit:BBL unit:BBL_UK_PET unit:BBL_US unit:CentiM3 unit:DecaL unit:DecaM3 unit:DeciL unit:DeciM3 unit:FBM unit:FT3 unit:FemtoL unit:GI_UK unit:GI_US unit:GT unit:HectoL unit:IN3 unit:Kilo-FT3 unit:KiloL unit:L unit:M3 unit:MI3 unit:MegaL unit:MicroL unit:MicroM3 unit:MilliL unit:MilliM3 unit:NanoL unit:OZ_VOL_UK unit:PINT unit:PINT_UK unit:PK_UK unit:PicoL unit:PlanckVolume unit:QT_UK unit:QT_US unit:RT unit:STR unit:Standard unit:TBSP unit:TON_SHIPPING_US unit:TSP unit:YD3 ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:AC-FT rdfs:label "Acre Foot"@en .
unit:ANGSTROM3 rdfs:label "Cubic Angstrom"@en, "Cubic Angstrom"@en-us .
unit:BBL rdfs:label "Barrel"@en .
unit:BBL_UK_PET rdfs:label "Barrel (Uk Petroleum)"@en .
unit:BBL_US rdfs:label "Barrel (Us)"@en .
unit:CentiM3 rdfs:label "Cubic Centimeter"@en-us, "Cubic Centimetre"@en .
unit:DecaL rdfs:label "Decalitre"@en, "Decalitre"@en-us .
unit:DecaM3 rdfs:label "Cubic Decameter"@en-us, "Cubic Decametre"@en .
unit:DeciL rdfs:label "Decilitre"@en, "Decilitre"@en-us .
unit:DeciM3 rdfs:label "Cubic Decimeter"@en-us, "Cubic Decimetre"@en .
unit:FBM rdfs:label "Board Foot"@en .
unit:FT3 rdfs:label "Cubic Foot"@en .
unit:FemtoL rdfs:label "Femtolitre"@en, "Femtolitre"@en-us .
unit:GI_UK rdfs:label "Gill (Uk)"@en .
unit:GI_US rdfs:label "Gill (Us)"@en .
unit:GT rdfs:label "Gross Tonnage"@en .
unit:HectoL rdfs:label "Hectolitre"@en, "Hectolitre"@en-us .
unit:IN3 rdfs:label "Cubic Inch"@en .
unit:Kilo-FT3 rdfs:label "Thousand Cubic Foot"@en .
unit:KiloL rdfs:label "Kilolitre"@en, "Kilolitre"@en-us .
unit:L rdfs:label "Liter"@en-us, "Litre"@en .
unit:M3 rdfs:label "Cubic Meter"@en-us, "Kubikmeter"@de, "Cubic Metre"@en, "Kubični Meter"@sl, "Meter Kubik"@ms, "Metr Krychlový"@cs, "Metr Sześcienny"@pl, "Metreküp"@tr, "Metro Cubo"@it, "Metro Cúbico"@es, "Metro Cúbico"@pt, "Metru Cub"@ro, "Metrum Cubicum"@la, "Mètre Cube"@fr, "Κυβικό Μετρο"@el, "Кубичен Метър"@bg, "Кубический Метр"@ru, "מטר מעוקב"@he, "متر مكعب"@ar, "متر مکعب"@fa, "घन मीटर"@hi, "立方メートル"@ja, "立方米"@zh .
unit:MI3 rdfs:label "Cubic Mile"@en .
unit:MegaL rdfs:label "Megalitre"@en, "Megalitre"@en-us .
unit:MicroL rdfs:label "Microlitre"@en, "Microlitre"@en-us .
unit:MicroM3 rdfs:label "Cubic Micrometres (Microns)"@en .
unit:MilliL rdfs:label "Millilitre"@en, "Millilitre"@en-us .
unit:MilliM3 rdfs:label "Cubic Millimeter"@en-us, "Cubic Millimetre"@en .
unit:NanoL rdfs:label "Nanolitre"@en, "Nanolitre"@en-us .
unit:OZ_VOL_UK rdfs:label "Fluid Ounce (Uk)"@en .
unit:PINT rdfs:label "Imperial Pint"@en .
unit:PINT_UK rdfs:label "Pint (Uk)"@en .
unit:PK_UK rdfs:label "Peck (Uk)"@en .
unit:PicoL rdfs:label "Picolitre"@en, "Picolitre"@en-us .
unit:PlanckVolume rdfs:label "Planck Volume"@en .
unit:QT_UK rdfs:label "Quart (Uk)"@en .
unit:QT_US rdfs:label "Us Liquid Quart"@en .
unit:RT rdfs:label "Register Ton"@en .
unit:STR rdfs:label "Stere"@en .
unit:Standard rdfs:label "Standard"@en .
unit:TBSP rdfs:label "Tablespoon"@en .
unit:TON_SHIPPING_US rdfs:label "Ton (Us Shipping)"@en .
unit:TSP rdfs:label "Teaspoon"@en .
unit:YD3 rdfs:label "Cubic Yard"@en .
