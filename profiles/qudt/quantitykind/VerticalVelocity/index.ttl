@base <https://purl.org/coscine/ap/qudt/quantitykind/VerticalVelocity/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/VerticalVelocity/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Vertical Velocity sind."@de,
    "Profile for all units that are a/an Vertical Velocity."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Vertical Velocity"@de,
    "Unit: Vertical Velocity"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:CentiM-PER-HR unit:CentiM-PER-KiloYR unit:CentiM-PER-SEC unit:CentiM-PER-YR unit:FT-PER-DAY unit:FT-PER-HR unit:FT-PER-MIN unit:FT-PER-SEC unit:GigaHZ-M unit:IN-PER-MIN unit:IN-PER-SEC unit:KN unit:KiloM-PER-DAY unit:KiloM-PER-HR unit:KiloM-PER-SEC unit:M-PER-HR unit:M-PER-MIN unit:M-PER-SEC unit:M-PER-YR unit:MI-PER-HR unit:MI-PER-MIN unit:MI-PER-SEC unit:MI_N-PER-HR unit:MI_N-PER-MIN unit:MilliM-PER-DAY unit:MilliM-PER-HR unit:MilliM-PER-MIN unit:MilliM-PER-SEC unit:MilliM-PER-YR ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:CentiM-PER-HR rdfs:label "Centimeter Per Hour"@en-us, "Centimetre Per Hour"@en .
unit:CentiM-PER-KiloYR rdfs:label "Centimetres Per Thousand Years"@en .
unit:CentiM-PER-SEC rdfs:label "Centimeter Per Second"@en-us, "Centimetre Per Second"@en .
unit:CentiM-PER-YR rdfs:label "Centimetres Per Year"@en .
unit:FT-PER-DAY rdfs:label "Foot Per Day"@en .
unit:FT-PER-HR rdfs:label "Foot Per Hour"@en .
unit:FT-PER-MIN rdfs:label "Foot Per Minute"@en .
unit:FT-PER-SEC rdfs:label "Foot Per Second"@en .
unit:GigaHZ-M rdfs:label "Gigahertz Meter"@en-us, "Gigahertz Metre"@en .
unit:IN-PER-MIN rdfs:label "Inch Per Minute"@en .
unit:IN-PER-SEC rdfs:label "Inch Per Second"@en .
unit:KN rdfs:label "Knot"@en .
unit:KiloM-PER-DAY rdfs:label "Kilometres Per Day"@en .
unit:KiloM-PER-HR rdfs:label "Kilometer Per Hour"@en-us, "Kilometre Per Hour"@en .
unit:KiloM-PER-SEC rdfs:label "Kilometer Per Second"@en-us, "Kilometre Per Second"@en .
unit:M-PER-HR rdfs:label "Meter Per Hour"@en-us, "Metre Per Hour"@en .
unit:M-PER-MIN rdfs:label "Meter Per Minute"@en-us, "Metre Per Minute"@en .
unit:M-PER-SEC rdfs:label "Meter Je Sekunde"@de, "Meter Per Second"@en-us, "Meter Na Sekundo"@sl, "Meter Per Saat"@ms, "Metr Na Sekundę"@pl, "Metr Za Sekundu"@cs, "Metra Per Secundum"@la, "Metre Bölü Saniye"@tr, "Metre Per Second"@en, "Metro Al Secondo"@it, "Metro Por Segundo"@es, "Metro Por Segundo"@pt, "Metru Pe Secundă"@ro, "Mètre Par Seconde"@fr, "Μέτρο Ανά Δευτερόλεπτο"@el, "Метр В Секунду"@ru, "Метър В Секунда"@bg, "מטרים לשנייה"@he, "متر بر ثانیه"@fa, "متر في الثانية"@ar, "मीटर प्रति सैकिण्ड"@hi, "メートル毎秒"@ja, "米每秒"@zh .
unit:M-PER-YR rdfs:label "Metres Per Year"@en .
unit:MI-PER-HR rdfs:label "Mile Per Hour"@en .
unit:MI-PER-MIN rdfs:label "Mile Per Minute"@en .
unit:MI-PER-SEC rdfs:label "Mile Per Second"@en .
unit:MI_N-PER-HR rdfs:label "Nautical Mile Per Hour"@en .
unit:MI_N-PER-MIN rdfs:label "Nautical Mile Per Minute"@en .
unit:MilliM-PER-DAY rdfs:label "Millimeters Per Day"@en-us, "Millimetres Per Day"@en .
unit:MilliM-PER-HR rdfs:label "Millimeter Per Hour"@en-us, "Millimetre Per Hour"@en .
unit:MilliM-PER-MIN rdfs:label "Millimeter Per Minute"@en-us, "Millimetre Per Minute"@en .
unit:MilliM-PER-SEC rdfs:label "Millimeter Per Second"@en-us, "Millimetre Per Second"@en .
unit:MilliM-PER-YR rdfs:label "Millimeter Per Year"@en-us, "Millimetre Per Year"@en .
