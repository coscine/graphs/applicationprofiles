@base <https://purl.org/coscine/ap/qudt/quantitykind/TotalLinearStoppingPower/> .

@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix coscinetype: <https://purl.org/coscine/terms/types#> .

<https://purl.org/coscine/ap/qudt/quantitykind/TotalLinearStoppingPower/>
  a sh:NodeShape, rdfs:Class, coscinetype:Module ;
  dcterms:created "2023-10-12"^^xsd:date;
  dcterms:modified "2023-10-19"^^xsd:date;
  dcterms:creator "Benedikt Heinrichs";
  dcterms:description "Profil für alle Einheiten, die eine Total Linear Stopping Power sind."@de,
    "Profile for all units that are a/an Total Linear Stopping Power."@en;
  dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
  dcterms:title "Einheit: Total Linear Stopping Power"@de,
    "Unit: Total Linear Stopping Power"@en;

  sh:property
    [
      sh:path qudt:unit ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:name "Unit"@en , "Einheit"@de ;
      sh:in ( unit:ERG-PER-CentiM unit:EV-PER-ANGSTROM unit:EV-PER-M unit:J-PER-M ) ;
    ] ;
  sh:property
    [
      sh:path qudt:value ;
      sh:minCount 1 ;
      sh:maxCount 1 ;
      sh:datatype xsd:decimal ;
      sh:name "Value"@en , "Wert"@de ;
      sh:description "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
      sh:message "Entry must be an integer or a decimal with a point separator (e.g., 2.5)."@en, "Die Eingabe muss eine ganze Zahl oder eine Dezimalzahl mit einem Punkttrennzeichen sein (z. B. 2.5)."@de ;
    ] ;
.

unit:ERG-PER-CentiM rdfs:label "Erg Per Centimeter"@en-us, "Erg Per Centimetre"@en .
unit:EV-PER-ANGSTROM rdfs:label "Electronvolt Per Angstrom"@en .
unit:EV-PER-M rdfs:label "Electronvolt Per Meter"@en-us, "Electronvolt Per Metre"@en .
unit:J-PER-M rdfs:label "Joule Per Meter"@en-us, "Joule Per Metre"@en .
