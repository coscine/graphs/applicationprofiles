@base <https://purl.org/coscine/ap/human_genetics/gen_seq_file_reference_information>.

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix sh: <http://www.w3.org/ns/shacl#>.
@prefix vocabterms: <https://purl.org/coscine/terms/>.
@prefix aps: <https://purl.org/coscine/ap/>.

<https://purl.org/coscine/ap/human_genetics/gen_seq_file_reference_information> dcterms:created "2024-08-10"^^xsd:date;
                                                      dcterms:creator "Florian Kraft";
                                                      dcterms:description "Specify the sequencing data which is avaiable for a sample. Eg FASTQ, VCF, BAM etc"@en;
                                                      dcterms:license <http://spdx.org/licenses/CC0-1.0>;
                                                      dcterms:subject <https://github.com/tibonto/dfgfo/205-03>;
                                                      dcterms:title "Genome sequencing file and reference information"@en;
                                                      a rdfs:Class,
                                                        sh:NodeShape;
                                                      sh:closed true;
                                                      sh:property [sh:description "Specify the reference genome which was used for data alignment"@en ; 
                                                                   sh:in ("hg19/GRCh37"                                                                          "hg38/GRCh38"                                                                          "CHM13/T2T") ; 
                                                                   sh:maxCount 1  ; 
                                                                   sh:name "Reference Genome"@de ; 
                                                                   sh:name "Reference Genome"@en ; 
                                                                   sh:order 1  ; 
                                                                   sh:path <https://purl.org/coscine/terms/072b5429-b321-4446-aa57-926767f7518d/>],
                                                                  [sh:description "If information is available what build and patch version was exactly used, please specify here"@en ; 
                                                                   sh:name "Reference genome details"@de ; 
                                                                   sh:name "Reference genome details"@en ; 
                                                                   sh:order 2  ; 
                                                                   sh:path <https://purl.org/coscine/terms/a70d13e0-a3bf-4004-8ce8-f95bd84d258f/> ; 
                                                                   sh:datatype xsd:string],
                                                                  [sh:description "specify if FASTQ files are available for the sample"@en ; 
                                                                   sh:maxCount 1  ; 
                                                                   sh:name "FASTQs"@de ; 
                                                                   sh:name "FASTQs"@en ; 
                                                                   sh:order 3  ; 
                                                                   sh:path <https://purl.org/coscine/terms/ae7ecd1a-9b69-44ea-b290-d9ba1af98197/> ; 
                                                                   sh:datatype xsd:boolean ; 
                                                                   sh:minCount 1 ],
                                                                  [sh:description "specify if uBAM (unmapped BAM file) files are available for the sample"@en ; 
                                                                   sh:maxCount 1  ; 
                                                                   sh:name "uBAM"@de ; 
                                                                   sh:name "uBAM"@en ; 
                                                                   sh:order 4  ; 
                                                                   sh:path <https://purl.org/coscine/terms/b7d4e8c5-7fb3-482d-bb84-2f94c07f8c89/> ; 
                                                                   sh:datatype xsd:boolean ; 
                                                                   sh:minCount 1 ],
                                                                  [sh:description "specify if BAM files are available for the sample"@en ; 
                                                                   sh:maxCount 1  ; 
                                                                   sh:name "BAM"@de ; 
                                                                   sh:name "BAM"@en ; 
                                                                   sh:order 5  ; 
                                                                   sh:path <https://purl.org/coscine/terms/1d28f909-bf22-4f7c-95ae-7531be17edad/> ; 
                                                                   sh:datatype xsd:boolean ; 
                                                                   sh:minCount 1 ],
                                                                  [sh:description "specify, if the uBAM or BAM files containing methylation information"@en ; 
                                                                   sh:maxCount 1  ; 
                                                                   sh:name "Methylation calls"@de ; 
                                                                   sh:name "Methylation calls"@en ; 
                                                                   sh:order 6  ; 
                                                                   sh:path <https://purl.org/coscine/terms/4afcf4d8-e234-4f94-82eb-df2c7d59dfa3/> ; 
                                                                   sh:datatype xsd:boolean ; 
                                                                   sh:minCount 1 ],
                                                                  [sh:description "specify if VCF files with SNV/InDel calls are available for the sample"@en ; 
                                                                   sh:maxCount 1  ; 
                                                                   sh:name "VCF SNV"@de ; 
                                                                   sh:name "VCF SNV"@en ; 
                                                                   sh:order 7  ; 
                                                                   sh:path <https://purl.org/coscine/terms/f63b9bc4-2df9-4209-a925-60c0f2a3c881/> ; 
                                                                   sh:datatype xsd:boolean ; 
                                                                   sh:minCount 1 ],
                                                                  [sh:description "specify if VCF files with CNV calls are available for the sample"@en ; 
                                                                   sh:maxCount 1  ; 
                                                                   sh:name "VCF CNV"@de ; 
                                                                   sh:name "VCF CNV"@en ; 
                                                                   sh:order 8  ; 
                                                                   sh:path <https://purl.org/coscine/terms/0caf3ef9-6bef-4aed-b8bd-48aa69d0d5c8/> ; 
                                                                   sh:datatype xsd:boolean ; 
                                                                   sh:minCount 1 ],
                                                                  [sh:description "specify if VCF files with SV calls are available for the sample"@en ; 
                                                                   sh:maxCount 1  ; 
                                                                   sh:name "VCF SV"@de ; 
                                                                   sh:name "VCF SV"@en ; 
                                                                   sh:order 9  ; 
                                                                   sh:path <https://purl.org/coscine/terms/26fdc491-f958-4c7a-94d1-97705acfab8a/> ; 
                                                                   sh:datatype xsd:boolean ; 
                                                                   sh:minCount 1 ],
                                                                  [sh:description "specify if BED files with methylation calls are available for the sample"@en ; 
                                                                   sh:name "BED methyl file"@de ; 
                                                                   sh:name "BED methyl file"@en ; 
                                                                   sh:order 10  ; 
                                                                   sh:path <https://purl.org/coscine/terms/84b27044-2c6d-4277-af51-f4a9906c9037/> ; 
                                                                   sh:datatype xsd:boolean],
                                                                  [sh:path rdf:type].
