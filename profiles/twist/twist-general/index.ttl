@base <https://purl.org/coscine/ap/twist/twist-general>.

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix dbo: <http://dbpedia.org/ontology/>.
@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix sh: <http://www.w3.org/ns/shacl#>.
@prefix aps: <https://purl.org/coscine/ap/>.

<https://purl.org/coscine/ap/twist/twist-general> dcterms:created "2024-08-29"^^xsd:date;
                                                  dcterms:creator "Jonas Nothhelfer, Ross Knapman, Karin Everschor-Sitte";
                                                  dcterms:description "Das Anwendungsprofil des TWIST-Teams enthält grundlegende Metadaten zur Dokumentation und Auffindbarkeit von Projekten. Es umfasst Felder wie Projekttitel, Autoren, Datum, Software und Datenformate, um eine einfache Identifizierung und Nutzung der Projekte zu ermöglichen."@de,
                                                                      "The TWIST Team's application profile provides basic metadata for documenting and discovering projects. It includes fields such as project title, authors, date, software, and data formats to support easy identification and use."@en;
                                                  dcterms:license <http://spdx.org/licenses/CC0-1.0>;
                                                  dcterms:subject <https://github.com/tibonto/dfgfo/307-02>;
                                                  dcterms:title "TWIST-General"@de,
                                                                "TWIST-General"@en;
                                                  a rdfs:Class,
                                                    sh:NodeShape;
                                                  sh:closed false;
                                                  sh:property [sh:datatype xsd:string ; 
                                                               sh:description "Individuals who have significantly contributed to the project."@en ; 
                                                               sh:description "Personen, die maßgeblich zur Erstellung des Projekts beigetragen haben."@de ; 
                                                               sh:minCount 1  ; 
                                                               sh:name "Authors"@en ; 
                                                               sh:name "Autoren"@de ; 
                                                               sh:order 3  ; 
                                                               sh:path dbo:author],
                                                              [sh:datatype xsd:string ; 
                                                               sh:description "Die Person oder Institution, die für die Erstellung der Ressource verantwortlich ist."@de ; 
                                                               sh:description "The individual or organization responsible for creating the resource."@en ; 
                                                               sh:minCount 1  ; 
                                                               sh:name "Creator of the ressource"@en ; 
                                                               sh:name "Ersteller der Ressource"@de ; 
                                                               sh:order 2  ; 
                                                               sh:path <https://d-nb.info/standards/elementset/gnd#creator>],
                                                              [sh:datatype xsd:date ; 
                                                               sh:description "Das Veröffentlichungsdatum oder das Datum, an dem das Projekt abgeschlossen wurde."@de ; 
                                                               sh:description "The publication date or the date when the project was completed."@en ; 
                                                               sh:minCount 1  ; 
                                                               sh:name "Date"@en ; 
                                                               sh:name "Datum"@de ; 
                                                               sh:order 4  ; 
                                                               sh:path dbo:date],
                                                              [sh:datatype xsd:string ; 
                                                               sh:description "Der offizielle Titel des Projekts, der zur Identifizierung verwendet wird."@de ; 
                                                               sh:description "The official title of the project, used for identification."@en ; 
                                                               sh:minCount 1  ; 
                                                               sh:name "Project title"@en ; 
                                                               sh:name "Projekttitel"@de ; 
                                                               sh:order 1  ; 
                                                               sh:path dbo:title],
                                                              [sh:datatype xsd:string ; 
                                                               sh:description "Die für das Projekt verwendete Software oder Tools."@de ; 
                                                               sh:description "The software or tools used for the project."@en ; 
                                                               sh:minCount 1  ; 
                                                               sh:name "Software"@de ; 
                                                               sh:name "Software"@en ; 
                                                               sh:order 6  ; 
                                                               sh:path <https://w3id.org/mdo/provenance/SoftwareName>],
                                                              [sh:datatype xsd:string ; 
                                                               sh:description "Key terms that describe the project and support discoverability."@en ; 
                                                               sh:description "Wichtige Begriffe, die das Projekt beschreiben und die Auffindbarkeit unterstützen."@de ; 
                                                               sh:name "Keywords"@en ; 
                                                               sh:name "Schlüsselwörter"@de ; 
                                                               sh:order 5  ; 
                                                               sh:path <https://w3id.org/okn/o/sd#keywords>],
                                                              [sh:datatype xsd:string ; 
                                                               sh:description "Die Formate, in denen die Projektressourcen bereitgestellt werden (z. B. .py, .mx3, etc.)."@de ; 
                                                               sh:description "The formats in which the project resources are provided (e.g. .py, .mx3,etc.)."@en ; 
                                                               sh:minCount 1  ; 
                                                               sh:name "Data formats"@en ; 
                                                               sh:name "Datenformate"@de ; 
                                                               sh:order 7  ; 
                                                               sh:path dbo:format],
                                                              [sh:datatype xsd:string ; 
                                                               sh:description " Web links to relevant publications or project resources."@en ; 
                                                               sh:description "Weblinks zu relevanten Veröffentlichungen oder Projektressourcen."@de ; 
                                                               sh:name "Link(s) to publication"@en ; 
                                                               sh:name "Link(s) zur Veröffentlichung"@de ; 
                                                               sh:order 8  ; 
                                                               sh:path <http://www.w3id.org/ecsel-dr-PMV#links>],
                                                              [sh:datatype xsd:string ; 
                                                               sh:description "Ergänzende Details, die für das Projekt relevant sind, aber nicht in andere Felder passen."@de ; 
                                                               sh:description "Supplementary details relevant to the project but not covered by other fields."@en ; 
                                                               sh:name "Further Information"@en ; 
                                                               sh:name "Weitere Informationen"@de ; 
                                                               sh:order 9  ; 
                                                               sh:path dbo:notes].
