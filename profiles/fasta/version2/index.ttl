@base <https://purl.org/coscine/ap/fasta/version2/>.

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix prov: <http://www.w3.org/ns/prov#>.
@prefix sh: <http://www.w3.org/ns/shacl#>.
@prefix vocabterms: <https://purl.org/coscine/terms/>.
@prefix aps: <https://purl.org/coscine/ap/>.

<https://purl.org/coscine/ap/fasta/version2/> dcterms:created "2022-08-25"^^xsd:date;
                                              dcterms:creator "Charlie Pauvert";
                                              dcterms:description "A set of metadata to describe FASTA sequences (e.g., genomic sequences) produced by processing raw sequences from omics technologies. This application profile was made with the minimal accepted metadata standard for FASTA files from the NFDI4Microbiota (https://nfdi4microbiota.de/)"@en,
                                                                  "Ein Satz von Metadaten zur Beschreibung von FASTA-Sequenzen (z. B. genomische Sequenzen), die durch die Verarbeitung von Rohsequenzen aus Omics-Technologien erzeugt wurden. Dieses Anwendungsprofil wurde mit dem minimalen akzeptierten Metadatenstandard für FASTA-Dateien der NFDI4Microbiota (https://nfdi4microbiota.de/) erstellt."@de;
                                              dcterms:license <https://creativecommons.org/publicdomain/zero/1.0/legalcode>;
                                              dcterms:rights "Copyright © 2022 Institute of Medical Microbiology, University Hospital of RWTH, Aachen";
                                              dcterms:subject <https://github.com/tibonto/dfgfo/204>;
                                              dcterms:title "FASTA Version 2"@de,
                                                            "FASTA Version 2"@en;
                                              a sh:NodeShape;
                                              prov:wasRevisionOf aps:fasta;
                                              sh:closed "1"^^xsd:boolean;
                                              sh:property [sh:datatype xsd:string ; 
                                                           sh:description "Phylogenetic marker(s) for MAG assignation. Multiple markers should be separated with a comma."@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Marker for taxonomic identification"@en ; 
                                                           sh:name "Marker für die taxonomische Identifizierung"@de ; 
                                                           sh:order 1  ; 
                                                           sh:path <https://w3id.org/mixs/terms/tax_ident>],
                                                          [sh:datatype xsd:decimal ; 
                                                           sh:description "Completeness score (from 0 to 100%)"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Completeness"@en ; 
                                                           sh:name "Vollständigkeit"@de ; 
                                                           sh:order 13  ; 
                                                           sh:path <https://w3id.org/mixs/terms/compl_score> ; 
                                                           sh:maxCount 1  ; 
                                                           sh:maxInclusive 100  ; 
                                                           sh:minInclusive 0 ],
                                                          [sh:datatype xsd:int ; 
                                                           sh:description "Total number of tRNAs identified"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Anzahl der tRNAs"@de ; 
                                                           sh:name "Number of tRNAs"@en ; 
                                                           sh:order 11  ; 
                                                           sh:path <https://w3id.org/mixs/terms/trnas> ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:decimal ; 
                                                           sh:description "Tool(s) used in contamination screening"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Contamination"@en ; 
                                                           sh:name "Kontamination"@de ; 
                                                           sh:order 15  ; 
                                                           sh:path <https://w3id.org/mixs/terms/contam_score> ; 
                                                           sh:maxCount 1  ; 
                                                           sh:minInclusive 0 ],
                                                          [sh:datatype xsd:int ; 
                                                           sh:description "Total number of contigs"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Anzahl von Contigs"@de ; 
                                                           sh:name "Number of Contigs"@en ; 
                                                           sh:order 5  ; 
                                                           sh:path <https://w3id.org/mixs/terms/number_contig> ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:decimal ; 
                                                           sh:description "The length of the shortest contig representing half of the assembly length"@en ; 
                                                           sh:name "N50"@de ; 
                                                           sh:name "N50"@en ; 
                                                           sh:order 6  ; 
                                                           sh:path vocabterms:N50],
                                                          [sh:datatype xsd:boolean ; 
                                                           sh:description "Detection of the 16S rRNA (Bacteria, Archaea) or 18S rRNA (Eukaryotes)"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Kleine ribosomale Untereinheit (z. B. 16S) wiedergefunden"@de ; 
                                                           sh:name "Small ribosomal Sub-Unit (e.g. 16S) recovered"@en ; 
                                                           sh:order 9  ; 
                                                           sh:path <https://w3id.org/mixs/terms/SSU_recover> ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:string ; 
                                                           sh:description "Tool(s) used for assembly, including version and parameters"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Assembly Software"@en ; 
                                                           sh:name "Montage-Software"@de ; 
                                                           sh:order 3  ; 
                                                           sh:path <https://w3id.org/mixs/terms/assembly_software> ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:boolean ; 
                                                           sh:description "Detection of the 23S rRNA (Bacteria, Archaea) or 5.8S/28S rRNA (Eukaryotes)"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Große ribosomale Untereinheit (z. B. 23S) wiedergefunden"@de ; 
                                                           sh:name "Large ribosomal Sub-Unit (e.g. 23S) recovered"@en ; 
                                                           sh:order 7  ; 
                                                           sh:path <https://w3id.org/mixs/terms/LSU_recover> ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:string ; 
                                                           sh:description "Tools used for LSU extraction"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "LSU Recover Software"@de ; 
                                                           sh:name "LSU Recover Software"@en ; 
                                                           sh:order 8  ; 
                                                           sh:path vocabterms:LSU_recover_software ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:decimal ; 
                                                           sh:description "The estimated depth of sequencing coverage (in x)."@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Coverage"@en ; 
                                                           sh:name "Erfassungsbereich"@de ; 
                                                           sh:order 4  ; 
                                                           sh:path vocabterms:coverage ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:string ; 
                                                           sh:description "Accessions/identifiers linking to the raw data (FASTQ)"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "FASTQ accession"@en ; 
                                                           sh:name "FASTQ-Zugang"@de ; 
                                                           sh:order 1  ; 
                                                           sh:path vocabterms:run_ref ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:string ; 
                                                           sh:description "Tools used for completion estimate"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Completeness Software"@en ; 
                                                           sh:name "Vollständigkeitssoftware"@de ; 
                                                           sh:order 14  ; 
                                                           sh:path <https://w3id.org/mixs/terms/compl_software> ; 
                                                           sh:maxCount 1 ],
                                                          [sh:description "The assembly quality category is based on sets of criteria outlined for each assembly quality category. For MISAG/MIMAG; Finished: Single, validated, contiguous sequence per replicon without gaps or ambiguities with a consensus error rate equivalent to Q50 or better. High Quality Draft:Multiple fragments where gaps span repetitive regions. Presence of the 23S, 16S and 5S rRNA genes and at least 18 tRNAs. Medium Quality Draft:Many fragments with little to no review of assembly other than reporting of standard assembly statistics. Low Quality Draft:Many fragments with little to no review of assembly other than reporting of standard assembly statistics. Assembly statistics include, but are not limited to total assembly size, number of contigs, contig N50/L50, and maximum contig length. For MIUVIG; Finished: Single, validated, contiguous sequence per replicon without gaps or ambiguities, with extensive manual review and editing to annotate putative gene functions and transcriptional units. High-quality draft genome: One or multiple fragments, totaling ≥ 90% of the expected genome or replicon sequence or predicted complete. Genome fragment(s): One or multiple fragments, totalling < 90% of the expected genome or replicon sequence, or for which no genome size could be estimated"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Assembly Quality"@en ; 
                                                           sh:name "Qualität der Montage"@de ; 
                                                           sh:order 2  ; 
                                                           sh:path <https://w3id.org/mixs/terms/assembly_qual> ; 
                                                           sh:maxCount 1  ; 
                                                           sh:in ("Finished genome"                                                                  "High-quality draft genome"                                                                  "Medium-quality draft genome"                                                                  "Low-quality draft genome"                                                                  "Genome fragment(s)")],
                                                          [sh:datatype xsd:string ; 
                                                           sh:description "Organisation, Agentur, Firma, die die Metadatensammlung unterstützt hat"@de ; 
                                                           sh:description "Organization, agency, company who has supported the Metadata collection"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "Supported by"@en ; 
                                                           sh:name "Unterstützt von"@de ; 
                                                           sh:order 16  ; 
                                                           sh:path <https://purl.org/coscine/terms/9f6e22e5-6b18-4d71-9d47-7ae43175667f/>],
                                                          [sh:datatype xsd:string ; 
                                                           sh:description "Tools used for LSU extraction"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "SSU Recover Software"@de ; 
                                                           sh:name "SSU Recover Software"@en ; 
                                                           sh:order 10  ; 
                                                           sh:path vocabterms:SSU_recover_software ; 
                                                           sh:maxCount 1 ],
                                                          [sh:datatype xsd:string ; 
                                                           sh:description "Tools used for tRNA identification"@en ; 
                                                           sh:minCount 1  ; 
                                                           sh:name "tRNAs Extraction Software"@en ; 
                                                           sh:name "tRNAs-Extraktionssoftware"@de ; 
                                                           sh:order 12  ; 
                                                           sh:path <https://w3id.org/mixs/terms/trna_ext_software> ; 
                                                           sh:maxCount 1 ];
                                              sh:targetClass aps:fasta.
