@base <https://purl.org/coscine/ap/preclinical-imaging/1677499481773/> .

@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix vocabterms: <https://purl.org/coscine/terms/> .
@prefix aps: <https://purl.org/coscine/ap/> .

<https://purl.org/coscine/ap/preclinical-imaging/1677499481773/> dcterms:created "2023-02-02"^^xsd:date ;
	dcterms:creator "Catherine Gonzalez" ;
	dcterms:description "Preclinical Imaging of animal specimens"@en, "Präklinische Bildgebung von Tierproben"@de ;
	dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html" ;
	dcterms:subject "Lebenswissenschaften"@de, "Life science "@en ;
	dcterms:title "Preclinical Imaging"@en, "Präklinische Bildgebung"@de ;
	a rdfs:Class, sh:NodeShape ;
	owl:imports <https://purl.org/coscine/ap/preclinical-imaging/MRI/parameters/>, <https://purl.org/coscine/ap/preclinical-imaging/CT/>, <https://purl.org/coscine/ap/preclinical-imaging/organ-segmentation/>;
	prov:wasRevisionOf aps:preclinical-imaging ;
	sh:closed false ;
	sh:property [
		sh:datatype xsd:string ;
		sh:description "Das Format einer elektronischen Datei. [NGI]"@de, "The format of an electronic file. [NCI]"@en ;
		sh:name "Data format"@en, "Datenformat"@de ;
		sh:order 12 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C171252> ;
	], [
		sh:name "Date type"@en, "Datentyp"@de ;
		sh:order 13 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C42645> ;
		sh:in (
			"Raw data|Rohdaten"
			"Reconstructed data|Rekonstruierte Daten"
		) ;
	], [
		sh:description "Art des Geräts, des Prozesses oder der Methode, die zur Erfassung oder Ableitung von Daten verwendet werden."@de, "Type of device, process or method used to acquire or derive data."@en ;
		sh:name "Bildgebungsmodalität"@de, "Image modality"@en ;
		sh:order 5 ;
		sh:path <http://dicom.nema.org/resources/ontology/DCM/121139> ;
		sh:in (
			"CT"
			"MRI"
			"US"
			"MRI-CT"
		) ;
		sh:message "Sie müssen 2 Werte aus der Liste auswählen"@de, "You must select 2 values from the list"@en ;
		sh:minCount 1 ;
		sh:severity sh:Warning ;
	], [
		sh:datatype xsd:string ;
		sh:name "Contrast agent name"@en, "Kontrastmittelname"@de ;
		sh:order 11 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C69294> ;
	], [
		sh:datatype xsd:string ;
		sh:description "Breed of animal (e.g. for breed of mouse the strain may be, C57BL/6, SKH1, BALB/c)"@de ;
		sh:name "Animal strain"@en, "Tierbelastung"@de ;
		sh:order 4 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C14419> ;
	], [
		sh:datatype xsd:string ;
		sh:description "e.g. maus35, Schwein2"@de, "e.g. mouse35, pig2"@en ;
		sh:name "Animal ID"@en, "Tier ID"@de ;
		sh:order 3 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C164820> ;
		sh:minCount 1 ;
	], [
		sh:name "MRI imaging parameters"@en ;
		sh:order 26 ;
		sh:path <https://purl.org/coscine/terms/0ce58f94-6638-4d59-9a5c-c190d725fba1/> ;
		sh:node <https://purl.org/coscine/ap/preclinical-imaging/MRI/parameters/> ;
	], [
		sh:name "CT imaging parameters"@en ;
		sh:order 25 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C115501> ;
		sh:node <https://purl.org/coscine/ap/preclinical-imaging/CT/> ;
	], [
		sh:description "Der Zustand des geistigen oder körperlichen Zustands eines Subjekts. [ NGI ]"@de, "The state of a subject's mental or physical condition. [ NCI ]"@en ;
		sh:name "Gesundheitszustand"@de, "Health status"@en ;
		sh:order 16 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C16669> ;
		sh:in (
			"Healthy|Gesund"
			"Not Healthy|nicht gesund"
		) ;
		sh:minCount 1 ;
		sh:maxCount 1 ;
	], [
		sh:description "A brand name is a trademarked and marketed name of a product."@en, "Ein Markenname ist ein geschützter und vermarkteter Name eines Produkts."@de ;
		sh:name "Brand"@en, "Marke"@de ;
		sh:order 6 ;
		sh:path <http://semanticscience.org/resource/SIO_000119> ;
		sh:in (
			"Bioemtech"
			"Bruker"
			"Fuji Film Visualsonics"
			"Mediso"
			"MILabs"
			"Molecubes"
			"MR Solutions"
			"PerkinElmer"
			"Spectral Instruments Imaging"
			"TriFoil Imaging"
			"other"
		) ;
		sh:minCount 1 ;
	], [
		sh:datatype xsd:string ;
		sh:description "A sequence of characters used to identify, name, or characterize the study."@en, "Eine Folge von Zeichen, die zum Identifizieren, Benennen oder Charakterisieren der Studie verwendet wird."@de ;
		sh:name "Studien-ID"@de, "Study ID"@en ;
		sh:order 2 ;
		sh:path <https://purl.org/coscine/terms/65369ab3-68b8-4fa7-9cba-d42af6860b66/> ;
		sh:minCount 1 ;
	], [
		sh:datatype xsd:string ;
		sh:description "Model Name of a device. (e.g. 7T BioSpec 70/20 USR)"@en, "Modellname eines Geräts. (z.B. 7T BioSpec 70/20 USR)"@de ;
		sh:name "Device name"@en, "Gerätename"@de ;
		sh:order 7 ;
		sh:path <http://dicom.nema.org/resources/ontology/DCM/113879> ;
	], [
		sh:datatype xsd:integer ;
		sh:description "A specific point in the time continuum, including those established relative to an event. [ NCI ]"@en, "Ein bestimmter Punkt im Zeitkontinuum, einschließlich derjenigen, die relativ zu einem Ereignis festgelegt wurden. [ NGI ]"@de ;
		sh:name "Time point"@en, "Zeitpunkt"@de ;
		sh:order 15 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C68568> ;
	], [
		sh:name "Preclinical Imaging-Organ Segmentation"@en ;
		sh:order 24 ;
		sh:path vocabterms:preclinical-imaging-organ-segmentation ;
		sh:node <https://purl.org/coscine/ap/preclinical-imaging/organ-segmentation/> ;
	], [
		sh:description "Die Ansammlung von physikalischen Eigenschaften oder Qualitäten, durch die männlich von weiblich unterschieden wird; der körperliche Unterschied zwischen männlich und weiblich; die unterscheidende Besonderheit von männlich oder weiblich. [ NCI aus dem Online-Medizinlexikon ]"@de, "The assemblage of physical properties or qualities by which male is distinguished from female; the physical difference between male and female; the distinguishing peculiarity of male or female. [ NCI from On-line Medical Dictionary ]"@en ;
		sh:name "Geschlecht"@de, "Sex"@en ;
		sh:order 20 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C28421> ;
		sh:in (
			"Female|weiblich"
			"Male|männlich"
		) ;
	], [
		sh:datatype xsd:string ;
		sh:name "Creator"@en, "Ersteller"@de ;
		sh:order 1 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C42628> ;
		sh:minCount 1 ;
		sh:maxCount 1 ;
	], [
		sh:datatype xsd:boolean ;
		sh:description "Längsschnittstudien"@de, "longitudinal studies"@en ;
		sh:name "Longitudinal data"@en ;
		sh:order 14 ;
		sh:path <http://purl.obolibrary.org/obo/OMIT_0009220> ;
		sh:minCount 1 ;
		sh:maxCount 1 ;
	], [
		sh:datatype xsd:string ;
		sh:description "A rodent disease whose pathologic mechanisms are sufficiently similar to those of a human disease to serve as a model. [ NCI ](e.g. liver fibrosis)"@en, "Eine Nagetierkrankheit, deren pathologische Mechanismen denen einer menschlichen Krankheit ausreichend ähnlich sind, um als Modell zu dienen. [ NGI ] (z.B. Leberfibrose)"@de ;
		sh:name "Disease model"@en, "Krankheitsmodell"@de ;
		sh:order 18 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C19021> ;
	], [
		sh:datatype xsd:string ;
		sh:description "Wenn MRT gewählt wird. Wenn MRT gewählt wird. Die Klassifizierung der Spule, die in einem Magnetresonanztomographieverfahren verwendet wird. Bezieht sich normalerweise auf die anatomische Stelle, an der die Spule platziert ist, wie Kopf, Körper oder Brust. [ NGI ] (z.B. RF Res 300 1H 112/086 QSN TO AD Volume Coil)"@de, "When MRI is chosen. The classification of the coil that is used in an magnetic resonance imaging procedure. Usually refers to the anatomical location for where the coil is placed such as head, body or breast. [ NCI ](e.g. RF Res 300 1H 112/086 QSN TO AD volume coil)"@en ;
		sh:name "Coil"@en, "Spule"@de ;
		sh:order 8 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C116119> ;
	], [
		sh:description "Sequenzname, Wiederholungszeit, Echozeit, Atemgating etc. Jeder Faktor, der ein System definiert und seine Leistung bestimmt (oder begrenzt). [ NGI ] (z.B. T2 TurboRare fws, 2715 ms, 22,6 ms, Ja)"@de, "sequence name, repetition time, echo time, respiration gating etc. Any factor that defines a system and determines (or limits) its performance. [ NCI ](e.g. T2 TurboRare fws, 2715 ms, 22.6 ms, Yes)"@en ;
		sh:name "Parameter"@de, "Parameters"@en ;
		sh:order 9 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C44175> ;
	], [
		sh:name "Quelle"@de, "Source"@en ;
		sh:order 10 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C25683> ;
		sh:in (
			"native|nativ"
			"contrast-enhanced|kontrastverstärkend"
		) ;
	], [
		sh:datatype xsd:integer ;
		sh:description "In tag. Wie lange gibt es schon etwas; verstrichene Zeit seit der Geburt. [NGI]"@de, "in days. How long something has existed; elapsed time since birth. [ NCI ]"@en ;
		sh:name "Age"@en, "Alter"@de ;
		sh:order 19 ;
		sh:path <http://purl.obolibrary.org/obo/NCIT_C25150> ;
		sh:message "Age cannot be zero or a negative number"@en, "Alter darf keine negative Zahl sein"@de ;
		sh:severity sh:Warning ;
		sh:flags "^\\d+$" ;
		sh:minExclusive 0 ;
	] .
