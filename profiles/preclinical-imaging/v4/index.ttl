@base <https://purl.org/coscine/ap/preclinical-imaging/v4>.

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix owl: <http://www.w3.org/2002/07/owl#>.
@prefix prov: <http://www.w3.org/ns/prov#>.
@prefix sh: <http://www.w3.org/ns/shacl#>.
@prefix vocabterms: <https://purl.org/coscine/terms/>.
@prefix aps: <https://purl.org/coscine/ap/>.

<https://purl.org/coscine/ap/preclinical-imaging/v4> dcterms:created "2023-02-02"^^xsd:date;
                                                     dcterms:creator "Catherine Gonzalez";
                                                     dcterms:description "Präklinische Bildgebung von Tierproben"@de,
                                                                         "Preclinical Imaging of animal specimens"@en;
                                                     dcterms:license <https://spdx.org/licenses/CC-BY-4.0.html>;
                                                     dcterms:subject <https://purl.org/coscine/ap/preclinical-imaging/Life%20science>;
                                                     dcterms:title "Präklinische Bildgebung V4"@de,
                                                                   "Preclinical Imaging Version 4"@en;
                                                     a rdfs:Class,
                                                       sh:NodeShape;
                                                     owl:imports <https://purl.org/coscine/ap/preclinical-imaging/CT/1713351361961/>,
                                                                 <https://purl.org/coscine/ap/preclinical-imaging/organ-segmentation/1713450156532/>,
                                                                 <https://purl.org/coscine/ap/preclinical-imaging/MRI/parameters/1714983627677/>;
                                                     prov:wasRevisionOf <https://purl.org/coscine/ap/preclinical-imaging/1681741010300/>;
                                                     sh:closed "0"^^xsd:boolean;
                                                     sh:property [sh:name "CT imaging parameters"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C115501> ; 
                                                                  sh:order 25  ; 
                                                                  sh:node <https://purl.org/coscine/ap/preclinical-imaging/CT/1713351361961/>],
                                                                 [sh:name "Geschlecht"@de ; 
                                                                  sh:name "Sex"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C28421> ; 
                                                                  sh:order 20  ; 
                                                                  sh:in ("Female|weiblich"                                                                         "Male|männlich") ; 
                                                                  sh:description "Die Ansammlung von physikalischen Eigenschaften oder Qualitäten, durch die männlich von weiblich unterschieden wird; der körperliche Unterschied zwischen männlich und weiblich; die unterscheidende Besonderheit von männlich oder weiblich. [ NCI aus dem Online-Medizinlexikon ]"@de ; 
                                                                  sh:description "The assemblage of physical properties or qualities by which male is distinguished from female; the physical difference between male and female; the distinguishing peculiarity of male or female. [ NCI from On-line Medical Dictionary ]"@en],
                                                                 [sh:name "Gerätename"@de ; 
                                                                  sh:name "Device name"@en ; 
                                                                  sh:path <http://dicom.nema.org/resources/ontology/DCM/113879> ; 
                                                                  sh:order 7  ; 
                                                                  sh:description "Modellname eines Geräts. (z.B. 7T BioSpec 70/20 USR)"@de ; 
                                                                  sh:description "Model Name of a device. (e.g. 7T BioSpec 70/20 USR)"@en ; 
                                                                  sh:datatype xsd:string],
                                                                 [sh:name "Longitudinal data"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/OMIT_0009220> ; 
                                                                  sh:order 15  ; 
                                                                  sh:description "longitudinal studies"@en ; 
                                                                  sh:description "Längsschnittstudien"@de ; 
                                                                  sh:datatype xsd:boolean ; 
                                                                  sh:maxCount 1  ; 
                                                                  sh:minCount 1 ],
                                                                 [sh:name "Preclinical Imaging-Organ Segmentation"@en ; 
                                                                  sh:path vocabterms:preclinical-imaging-organ-segmentation ; 
                                                                  sh:order 24  ; 
                                                                  sh:node <https://purl.org/coscine/ap/preclinical-imaging/organ-segmentation/1713450156532/>],
                                                                 [sh:name "Disease model"@en ; 
                                                                  sh:name "Krankheitsmodell"@de ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C19021> ; 
                                                                  sh:order 18  ; 
                                                                  sh:description "A rodent disease whose pathologic mechanisms are sufficiently similar to those of a human disease to serve as a model. [ NCI ](e.g. liver fibrosis)"@en ; 
                                                                  sh:description "Eine Nagetierkrankheit, deren pathologische Mechanismen denen einer menschlichen Krankheit ausreichend ähnlich sind, um als Modell zu dienen. [ NGI ] (z.B. Leberfibrose)"@de ; 
                                                                  sh:datatype xsd:string],
                                                                 [sh:name "Parameter"@de ; 
                                                                  sh:name "Parameters"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C44175> ; 
                                                                  sh:order 10  ; 
                                                                  sh:description "Sequenzname, Wiederholungszeit, Echozeit, Atemgating etc. Jeder Faktor, der ein System definiert und seine Leistung bestimmt (oder begrenzt). [ NGI ] (z.B. T2 TurboRare fws, 2715 ms, 22,6 ms, Ja)"@de ; 
                                                                  sh:description "sequence name, repetition time, echo time, respiration gating etc. Any factor that defines a system and determines (or limits) its performance. [ NCI ](e.g. T2 TurboRare fws, 2715 ms, 22.6 ms, Yes)"@en],
                                                                 [sh:name "Datentyp"@de ; 
                                                                  sh:name "Date type"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C42645> ; 
                                                                  sh:order 13  ; 
                                                                  sh:in ("Raw data|Rohdaten"                                                                         "Reconstructed data|Rekonstruierte Daten")],
                                                                 [sh:name "Study ID"@en ; 
                                                                  sh:name "Studien-ID"@de ; 
                                                                  sh:path <https://purl.org/coscine/terms/65369ab3-68b8-4fa7-9cba-d42af6860b66/> ; 
                                                                  sh:order 2  ; 
                                                                  sh:description "A sequence of characters used to identify, name, or characterize the study."@en ; 
                                                                  sh:description "Eine Folge von Zeichen, die zum Identifizieren, Benennen oder Charakterisieren der Studie verwendet wird."@de ; 
                                                                  sh:datatype xsd:string ; 
                                                                  sh:minCount 1 ],
                                                                 [sh:name "Animal ID"@en ; 
                                                                  sh:name "Tier ID"@de ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C164820> ; 
                                                                  sh:order 3  ; 
                                                                  sh:description "e.g. mouse35, pig2"@en ; 
                                                                  sh:description "e.g. maus35, Schwein2"@de ; 
                                                                  sh:datatype xsd:string ; 
                                                                  sh:minCount 1 ],
                                                                 [sh:name "Time point [min]"@en ; 
                                                                  sh:name "Zeitpunkt [min]"@de ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C68568> ; 
                                                                  sh:order 16  ; 
                                                                  sh:description "Time after injection."@en ; 
                                                                  sh:description "Zeit nach der Injektion."@de ; 
                                                                  sh:datatype xsd:integer],
                                                                 [sh:name "Quelle"@de ; 
                                                                  sh:name "Source"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C25683> ; 
                                                                  sh:order 11  ; 
                                                                  sh:in ("native|nativ"                                                                         "contrast-enhanced|kontrastverstärkend")],
                                                                 [sh:name "Image modality"@en ; 
                                                                  sh:name "Bildgebungsmodalität"@de ; 
                                                                  sh:path <http://dicom.nema.org/resources/ontology/DCM/121139> ; 
                                                                  sh:order 6  ; 
                                                                  sh:in ("CT"                                                                         "MRI"                                                                         "US"                                                                         "MRI-CT") ; 
                                                                  sh:description "Art des Geräts, des Prozesses oder der Methode, die zur Erfassung oder Ableitung von Daten verwendet werden."@de ; 
                                                                  sh:description "Type of device, process or method used to acquire or derive data."@en ; 
                                                                  sh:minCount 1  ; 
                                                                  sh:severity sh:Warning ; 
                                                                  sh:message "You must select 2 values from the list"@en ; 
                                                                  sh:message "Sie müssen 2 Werte aus der Liste auswählen"@de],
                                                                 [sh:name "Health status"@en ; 
                                                                  sh:name "Gesundheitszustand"@de ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C16669> ; 
                                                                  sh:order 17  ; 
                                                                  sh:in ("Healthy|Gesund"                                                                         "Not Healthy|nicht gesund") ; 
                                                                  sh:description "Der Zustand des geistigen oder körperlichen Zustands eines Subjekts. [ NGI ]"@de ; 
                                                                  sh:description "The state of a subject's mental or physical condition. [ NCI ]"@en ; 
                                                                  sh:maxCount 1  ; 
                                                                  sh:minCount 1 ],
                                                                 [sh:name "Coil"@en ; 
                                                                  sh:name "Spule"@de ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C116119> ; 
                                                                  sh:order 9  ; 
                                                                  sh:description "When MRI is chosen. The classification of the coil that is used in an magnetic resonance imaging procedure. Usually refers to the anatomical location for where the coil is placed such as head, body or breast. [ NCI ](e.g. RF Res 300 1H 112/086 QSN TO AD volume coil)"@en ; 
                                                                  sh:description "Wenn MRT gewählt wird. Wenn MRT gewählt wird. Die Klassifizierung der Spule, die in einem Magnetresonanztomographieverfahren verwendet wird. Bezieht sich normalerweise auf die anatomische Stelle, an der die Spule platziert ist, wie Kopf, Körper oder Brust. [ NGI ] (z.B. RF Res 300 1H 112/086 QSN TO AD Volume Coil)"@de ; 
                                                                  sh:datatype xsd:string],
                                                                 [sh:name "Brand"@en ; 
                                                                  sh:name "Marke"@de ; 
                                                                  sh:path <http://semanticscience.org/resource/SIO_000119> ; 
                                                                  sh:order 8  ; 
                                                                  sh:in ("Bioemtech"                                                                         "Bruker"                                                                         "Fuji Film Visualsonics"                                                                         "Mediso"                                                                         "MILabs"                                                                         "Molecubes"                                                                         "MR Solutions"                                                                         "PerkinElmer"                                                                         "Spectral Instruments Imaging"                                                                         "TriFoil Imaging"                                                                         "other") ; 
                                                                  sh:description "A brand name is a trademarked and marketed name of a product."@en ; 
                                                                  sh:description "Ein Markenname ist ein geschützter und vermarkteter Name eines Produkts."@de ; 
                                                                  sh:minCount 1 ],
                                                                 [sh:name "Contrast agent name"@en ; 
                                                                  sh:name "Kontrastmittelname"@de ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C69294> ; 
                                                                  sh:order 14  ; 
                                                                  sh:datatype xsd:string],
                                                                 [sh:name "Animal strain"@en ; 
                                                                  sh:name "Tierbelastung"@de ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C14419> ; 
                                                                  sh:order 4  ; 
                                                                  sh:description "Breed of animal (e.g. for breed of mouse the strain may be, C57BL/6, SKH1, BALB/c)"@de ; 
                                                                  sh:datatype xsd:string],
                                                                 [sh:name "Ersteller"@de ; 
                                                                  sh:name "Creator"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C42628> ; 
                                                                  sh:order 1  ; 
                                                                  sh:datatype xsd:string ; 
                                                                  sh:maxCount 1  ; 
                                                                  sh:minCount 1 ],
                                                                 [sh:name "MRI imaging parameters"@en ; 
                                                                  sh:path <https://purl.org/coscine/terms/0ce58f94-6638-4d59-9a5c-c190d725fba1/> ; 
                                                                  sh:order 26  ; 
                                                                  sh:node <https://purl.org/coscine/ap/preclinical-imaging/MRI/parameters/1714983627677/>],
                                                                 [sh:name "Alter"@de ; 
                                                                  sh:name "Age"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C25150> ; 
                                                                  sh:order 19  ; 
                                                                  sh:description "in days. How long something has existed; elapsed time since birth. [ NCI ]"@en ; 
                                                                  sh:description "In tag. Wie lange gibt es schon etwas; verstrichene Zeit seit der Geburt. [NGI]"@de ; 
                                                                  sh:datatype xsd:integer ; 
                                                                  sh:severity sh:Warning ; 
                                                                  sh:message "Age cannot be zero or a negative number"@en ; 
                                                                  sh:message "Alter darf keine negative Zahl sein"@de ; 
                                                                  sh:flags "^\\d+$" ; 
                                                                  sh:minExclusive 0 ],
                                                                 [sh:name "Datenformat"@de ; 
                                                                  sh:name "Data format"@en ; 
                                                                  sh:path <http://purl.obolibrary.org/obo/NCIT_C171252> ; 
                                                                  sh:order 12  ; 
                                                                  sh:description "Das Format einer elektronischen Datei. [NGI]"@de ; 
                                                                  sh:description "The format of an electronic file. [NCI]"@en ; 
                                                                  sh:datatype xsd:string],
                                                                 [sh:name "Supplier"@en ; 
                                                                  sh:name "Anbieter"@de ; 
                                                                  sh:path <https://purl.org/coscine/terms/e21c1d63-6b54-4f30-b6aa-ad1090a7e49c/> ; 
                                                                  sh:order 5  ; 
                                                                  sh:datatype xsd:string].
