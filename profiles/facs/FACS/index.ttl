@base <https://purl.org/coscine/ap/facs/FACS/>.

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix prov: <http://www.w3.org/ns/prov#>.
@prefix sh: <http://www.w3.org/ns/shacl#>.
@prefix vocabterms: <https://purl.org/coscine/terms/>.
@prefix aps: <https://purl.org/coscine/ap/>.

<https://purl.org/coscine/ap/facs#created> sh:datatype xsd:date;
                                           sh:defaultValue "{TODAY}";
                                           sh:maxCount 1 ;
                                           sh:minCount 1 ;
                                           sh:name "Date of Analysis"@en,
                                                   "Untersuchungsdatum"@de;
                                           sh:order 3 ;
                                           sh:path dcterms:created.
<https://purl.org/coscine/ap/facs#creator> sh:datatype xsd:string;
                                           sh:defaultValue "{ME}";
                                           sh:maxCount 1 ;
                                           sh:minCount 1 ;
                                           sh:minLength 1 ;
                                           sh:name "Creator"@en,
                                                   "Ersteller"@de;
                                           sh:order 0 ;
                                           sh:path dcterms:creator.
<https://purl.org/coscine/ap/facs#rights> sh:datatype xsd:string;
                                          sh:maxCount 1 ;
                                          sh:name "Berechtigung"@de,
                                                  "Rights"@en;
                                          sh:order 4 ;
                                          sh:path dcterms:rights.
<https://purl.org/coscine/ap/facs#subject> sh:datatype xsd:string;
                                           sh:maxCount 1 ;
                                           sh:name "Affiliation"@de,
                                                   "Affiliation"@en;
                                           sh:order 1 ;
                                           sh:path <http://purl.obolibrary.org/obo/NCIT_C154704>.
<https://purl.org/coscine/ap/facs/FACS/> dcterms:created "2023-09-05"^^xsd:date;
                                         dcterms:creator "Catherine Gonzalez";
                                         dcterms:description "Durchflusszytometrische Daten"@de,
                                                             "Flow Cytometry data"@en;
                                         dcterms:license "http://spdx.org/licenses/MIT";
                                         dcterms:publisher <https://itc.rwth-aachen.de/>;
                                         dcterms:rights "Copyright © 2022 IT Center, RWTH Aachen University";
                                         dcterms:title "Extended FACS - Fluorescence Activated Cell Sorting"@en,
                                                       "Erweitertes FACS - Fluoreszenzaktivierte Zellsortierung"@de;
                                         a sh:NodeShape;
                                         rdfs:comment "Application profile for data from fluorescence activated cell sorting (FACS)"@de,
                                                      "Application profile for data from fluorescence activated cell sorting (FACS)"@en;
                                         prov:wasRevisionOf <https://purl.org/coscine/ap/facs/>;
                                         sh:closed true;
                                         sh:property <https://purl.org/coscine/ap/facs#created>,
                                                     <https://purl.org/coscine/ap/facs#creator>,
                                                     <https://purl.org/coscine/ap/facs#rights>,
                                                     <https://purl.org/coscine/ap/facs#subject>,
                                                     [sh:datatype xsd:string ; 
                                                      sh:name "Hypothese"@de ; 
                                                      sh:name "Hypothesis"@en ; 
                                                      sh:order 5  ; 
                                                      sh:path <http://purl.obolibrary.org/obo/NCIT_C28362>],
                                                     [sh:datatype xsd:string ; 
                                                      sh:maxCount 1  ; 
                                                      sh:name "ORCID"@de ; 
                                                      sh:name "ORCID"@en ; 
                                                      sh:order 2  ; 
                                                      sh:path <http://purl.obolibrary.org/obo/APOLLO_SV_00000496>],
                                                     [sh:maxCount 1  ; 
                                                      sh:minCount 1  ; 
                                                      sh:name "Instrument"@de ; 
                                                      sh:name "Instrument"@en ; 
                                                      sh:order 6  ; 
                                                      sh:path <http://purl.obolibrary.org/obo/NCIT_C16742> ; 
                                                      sh:class <http://purl.org/coscine/vocabularies/facs/instruments>],
                                                     [sh:datatype xsd:string ; 
                                                      sh:name "Organism"@en ; 
                                                      sh:name "Organismus"@de ; 
                                                      sh:order 7  ; 
                                                      sh:path <http://purl.obolibrary.org/obo/NCIT_C14250>],
                                                     [sh:datatype xsd:string ; 
                                                      sh:name "Cell source"@en ; 
                                                      sh:name "Zellherkunft"@de ; 
                                                      sh:order 8  ; 
                                                      sh:path <http://purl.obolibrary.org/obo/NCIT_C158867>],
                                                     [sh:datatype xsd:string ; 
                                                      sh:minCount 1  ; 
                                                      sh:name "Analysierte Marker"@de ; 
                                                      sh:name "Markers analysed"@en ; 
                                                      sh:order 9  ; 
                                                      sh:path <http://purl.obolibrary.org/obo/NCIT_C16725>],
                                                     [sh:datatype xsd:integer ; 
                                                      sh:maxCount 1  ; 
                                                      sh:minCount 1  ; 
                                                      sh:name "Anzahl an Samples"@de ; 
                                                      sh:name "Number of Samples"@en ; 
                                                      sh:order 10  ; 
                                                      sh:path <http://purl.obolibrary.org/obo/NCIT_C25463>],
                                                     [sh:datatype xsd:string ; 
                                                      sh:name "Reference"@en ; 
                                                      sh:name "Referenz"@de ; 
                                                      sh:order 11  ; 
                                                      sh:path <http://purl.obolibrary.org/obo/NCIT_C19026>],
                                                     [sh:datatype xsd:string ; 
                                                      sh:name "Auxillary Data 1"@en ; 
                                                      sh:name "Ergänzende Daten 1"@de ; 
                                                      sh:order 12  ; 
                                                      sh:path <https://purl.org/coscine/terms/4d26780a-8fe2-46e0-b946-f968ab058d09/> ; 
                                                      sh:description "Andere einschlägige Metadaten"@de ; 
                                                      sh:description "Other pertinent metadata"@en],
                                                     [sh:datatype xsd:string ; 
                                                      sh:name "Ergänzende Daten 2"@de ; 
                                                      sh:name "Auxillary Data 2"@en ; 
                                                      sh:order 13  ; 
                                                      sh:path <https://purl.org/coscine/terms/20821e61-2bf1-4d69-a936-c044c5077508/> ; 
                                                      sh:description "Andere einschlägige Metadaten"@de ; 
                                                      sh:description "Other pertinent metadata"@en],
                                                     [sh:path rdf:type];
                                         sh:targetClass <https://purl.org/coscine/ap/facs/>.
